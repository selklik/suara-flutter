import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'ApiHelper.dart';
import 'Mutators.dart';
import 'dart:convert';
import 'Models.dart';

class ViewMore extends StatelessWidget
{
  final String responseString;

  ViewMore(this.responseString);

  @override
  Widget build(BuildContext context) {

    var decoded = json.decode(responseString);

    States states = States.fromJson(decoded);

    final screen = MediaQuery.of(context).size;

    return MaterialApp(home: Scaffold(

        appBar: AppBar(backgroundColor: Colors.white,
                       title: Container(child: Row(children: <Widget>[
                         GestureDetector(child: Icon(Icons.chevron_left, color: Colors.black, size: screen.height * 0.05,),
                         onTap: (){
                           Navigator.pop(context);

                         },),
                         Container(child: Center(child: Text(states.Kedah[0].state, style: TextStyle(color: Colors.black),
                         textAlign: TextAlign.center,),))],)),),

        body: ListView.builder(itemBuilder: (BuildContext cnt,int index){

          return Container(margin: EdgeInsets.only(top: screen.height* 0.02),height: screen.height * 0.15 ,child: AdapterList(mpsList: states.Kedah, position: index,), );

        },itemCount: states.Kedah.length) ,

      ),);
  }


}

class AdapterList extends StatelessWidget{

  final List<MpsPersonalInfo> mpsList;
  final int position;

  const AdapterList({Key key, this.mpsList, this.position}) : super (key: key);


  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Container(margin: EdgeInsets.all(screen.width * 0.02),
      child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
         // Avatar Ciruclar - Start Container //
        Container(height: screen.width * 0.15, width: screen.width * 0.15,
            decoration: BoxDecoration(image: new DecorationImage(
                image: new NetworkImage(mpsList[position].profile_photos.thumbnail),
                fit: BoxFit.fill), borderRadius:
                new BorderRadius.circular(50.0))
        ),

        // Names, account icons - Middle Container -- //
        Expanded(child: Container(margin: EdgeInsets.fromLTRB(screen.width * 0.05, 0.0, 0.0, 0.0),

          child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[

            Text(mpsList[position].name, style: TextStyle(fontSize: 16.0, color: Colors.black),),
            Row(children: <Widget>[
              Container(margin: EdgeInsets.fromLTRB(0.0, 3.0, 5.0, 0.0), child: Text(mpsList[position].country_name, style: TextStyle(fontSize: 13.0, color: Colors.grey),)),
              Container(margin: EdgeInsets.fromLTRB(5.0, 3.0, 0.0, 0.0), child: Text(mpsList[position].followers_count.toString() + " Followers", style: TextStyle(fontSize: 13.0, color: Colors.grey),)),

            ],),

            Expanded(child:
            //-- Icons Facebook, Twitter, Instagram
            Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Container(margin: EdgeInsets.fromLTRB(0.0, 6.0, 5.0, 0.0),
                child: mpsList[position].social_network_accounts.facebook != null ? Icon(FontAwesomeIcons.facebook, color: Colors.black, size: 20.0,) : Icon(FontAwesomeIcons.facebook, color: Colors.black12, size: 20.0,),),
              Container(margin: EdgeInsets.fromLTRB(6.0, 6.0, 5.0, 0.0),
                child: mpsList[position].social_network_accounts.twitter != null ? Icon(FontAwesomeIcons.twitterSquare, color: Colors.black, size: 20.0,) : Icon(FontAwesomeIcons.twitterSquare, color: Colors.black12, size: 20.0,),),
              Container(margin: EdgeInsets.fromLTRB(6.0, 6.0, 5.0, 0.0),
                child: mpsList[position].social_network_accounts.instagram != null ? Icon(FontAwesomeIcons.instagram, color: Colors.black, size: 20.0,) : Icon(FontAwesomeIcons.instagram, color: Colors.black12, size: 20.0,),),

            ],)),


          ],

        ),

        )),

        Container(child: FollowBtn(mpsList[position].id, mpsList[position].following))



    ],),);
  }
}

class FollowBtn extends StatefulWidget{

  int artistId;
  bool following;
  String access_token;

  FollowBtn(this.artistId, this.following);

  FollowBtnState createState() => new FollowBtnState(artistId, following);

}

class FollowBtnState extends State<FollowBtn>{

  int artistId;
  bool following;
  bool selfPressFollow = false;
  bool selfPressUnFollow = false;

  FollowBtnState(this.artistId, this.following);


  @override
  void initState() {
    super.initState();

    SuaraStorage().getString(Config().LoginDetails).then((value){

      setState(() {

        final jsonResponse = json.decode(value);
        PersonInfo myInfo = new PersonInfo.fromJson(jsonResponse);

        widget.access_token = myInfo.access_token;

      });

    });

  }

  @override
  Widget build(BuildContext context) {

    HashMap<String, String> ApiParams = new HashMap<String,String>();
                            ApiParams.putIfAbsent("access_token", ()=> widget.access_token);

    if (selfPressFollow){

        initApi(Config().API_BASE + "/artists/" + artistId.toString().trim() +"/follow", ApiParams, context, false).PostApi().then((response){

          var decoded = json.decode(response);

          if (decoded["follow"] == true){

            setState(() {

              following = true;
              selfPressFollow = false;
              selfPressUnFollow = false;

            });

          }

        });

    }

    if (selfPressUnFollow){

      initApi(Config().API_BASE + "/artists/" + artistId.toString().trim() +"/follow", ApiParams, context, false).DeleteApi().then((response){

        var decoded = json.decode(response);

        if (decoded["follow"] == false){

          setState(() {

            following = false;
            selfPressFollow = false;
            selfPressUnFollow = false;

          });

        }

      });

    }

    final screen = MediaQuery.of(context).size;

    final unfollowContainer = Container(height: double.infinity, margin: EdgeInsets.fromLTRB(0.0, screen.height * 0.01, 0.0, 0.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[

          new RaisedButton(color: Colors.red,
              child: new Container(width: screen.width * 0.2, height: screen.height * 0.04,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0)),
                child: Stack(children: <Widget>[
                  Container(child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Icon(Icons.star, color: Colors.white, size: screen.width * 0.04,)],)),
                  Container(child: Center(child: Text("Follow", style: TextStyle(color: Colors.white, fontFamily: "Regular"),)))

                ],),),
              onPressed: (){

                setState(() {

                  selfPressFollow = true;

                });

              }, shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
          ),
         /* GestureDetector(child: Container(width: screen.width * 0.25, height: screen.height * 0.05, child: InkWell(onTap: null,
            child: Container(decoration: new BoxDecoration(
              color: Colors.lightBlue,
              border: new Border.all(color: Colors.lightBlue, width: 0.5),
              borderRadius: new BorderRadius.circular(5.0),),

                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[Icon(Icons.check, size: 13.0, color: Colors.white,),
                Text("Follow", style: TextStyle(color: Colors.white, ), textAlign: TextAlign.center,)],

                )),
          )),onTap: (){

            setState(() {

              selfPressFollow = true;

            });

          },)*/],
        )
    );

    final followingContainer = GestureDetector(child: Container(height: double.infinity, margin: EdgeInsets.fromLTRB(0.0, screen.height * 0.01, 0.0, 0.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[

          Container(width: screen.width * 0.3, height: screen.height * 0.05, child: InkWell(onTap: null,
            child: Container(decoration: new BoxDecoration(
              color: Colors.white,
              border: new Border.all(color: Colors.black, width: 0.5),
              borderRadius: new BorderRadius.circular(15.0),),

                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[Icon(Icons.check, size: screen.width * 0.05, color: Colors.black,),
                Text("Following", style: TextStyle(color: Colors.black, ), textAlign: TextAlign.center,)],

              )),
          ),),],
        )
    ),  onTap: (){

          setState(() {

            selfPressUnFollow = true;

          });

    });

    return following ? followingContainer : unfollowContainer ;
  }
}