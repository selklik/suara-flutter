
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:suara/Models.dart';
import 'package:suara/Mutators.dart';
import 'package:suara/SpecificArtist.dart';
import 'package:suara/ViewMore.dart';
import 'ApiHelper.dart';




class Search extends StatefulWidget{

  String responseString = "";
  States states;
  Widget MpListBulder;

  String searchMpName = "";

  List<MpsPersonalInfo> mpsSearchInfo = new List();

  SearchState createState() => SearchState();

}
TextEditingController controller = new TextEditingController();

class SearchState extends State<Search>
{


  @override
  void initState() {
    super.initState();

    SuaraStorage().getString(Config().MpDetails).then((response){

      if (response != ""){

        setState(() {

          widget.responseString = response;
          var decoded = json.decode(response);
          widget.states = States.fromJson(decoded);

        });

      }

    });

  }
  HashMap<String, String> ApiParams;

  onSearchTextChanged(String text) async {

        widget.mpsSearchInfo.clear();

    if (text.isEmpty != null && text != "") {
        widget.searchMpName = text;

        for (int i = 0 ; i < widget.states.Kedah.length ; i++){

          if (widget.states.Kedah[i].name.startsWith(widget.searchMpName) || widget.states.Kedah[i].name.contains(widget.searchMpName)){
              widget.mpsSearchInfo.add(widget.states.Kedah[i]);
            }
        }

        setState(() {});


    } else {
        widget.searchMpName = "";
        widget.mpsSearchInfo.clear();
        setState(() {});
    }

  }

  @override
  Widget build(BuildContext context) {

    var screen = MediaQuery.of(context).size;

    final TopSearchBar = new Container(height: screen.height * 0.1, color: Colors.white,

        child: Container(margin : EdgeInsets.all(5.0),

            child: new Theme(
                data: new ThemeData(
                  primaryColor: Colors.red,
                ),
                  child: new TextField(
                  onChanged: onSearchTextChanged,
                  controller: controller,
                  decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                    borderRadius: BorderRadius.circular(screen.width * 0.08),
                      borderSide: new BorderSide(color: Colors.teal)),
                  hintText: 'Search your mp/adun and start following',
                  labelText: "Search Mp/Adun",
                  prefixIcon: const Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  suffixStyle: const TextStyle(color: Colors.green)),
            )),
        ));


    if (widget.responseString == ""){

        _initRefresh();

    } else {

      widget.searchMpName.isEmpty?
            widget.MpListBulder = new Expanded(child: new RefreshIndicator(child: ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int Index) {
            return Container(
                height: screen.height * 0.2, width: screen.width,
                margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                child: Card(elevation: 3.0,
                color: Colors.white,
                child: new ArtistWidgetSearch(
                  mpInfo: widget.states.Kedah,
                  position: Index,
                  responseString: widget.responseString
                      .trim(),),),);

                }), onRefresh: _initRefresh)) :

                  widget.mpsSearchInfo.length > 0 ? widget.MpListBulder =  new Expanded(
                      child: ListView.builder(itemCount: widget.mpsSearchInfo.length, itemBuilder: (BuildContext cnt,int index){
                        return Container(margin: EdgeInsets.only(top: screen.height* 0.02),height: screen.height * 0.15,
                          child: AdapterList(mpsList: widget.mpsSearchInfo, position: index,), );

                })) : widget.MpListBulder =  new Expanded(child: Container(child: Center(child: Text("Sorry, No Mp/Adun found !", style: TextStyle(color: Colors.black, fontFamily: "Bold"),),),));
    }


    return Container(child: Column(
      children: <Widget>[

        TopSearchBar,
        widget.responseString != "" && widget.MpListBulder != null ? widget.MpListBulder
                    : new Expanded(child: Container(width: screen.width, child: Center(child: showCircularIndicator()), ))

      ],
    ));
  }


  Future _initRefresh() async {

    ApiParams = new HashMap();
    ApiParams.putIfAbsent("access_token", () => "seuR9NQ6rNNFmx1iSX53UkH9");

    initApi(Config().API_BASE + Config().MODULE_MPS_LIST, ApiParams, context, false).GetApi().then((responseString){

      setState(() {

        if (responseStatus == StatusOkay && responseString != ""){
          
           widget.responseString = responseString;
           var decoded = json.decode(responseString);
           widget.states = States.fromJson(decoded);

          }

      });

    });

    Completer<Null> completer = new Completer<Null>();
    
    new Future.delayed(new Duration(seconds: 3)).then((_){

        completer.complete();

    });

    return completer.future;
  }
}

class ArtistWidgetSearch extends StatelessWidget{

  final int position;
  final String responseString;
  final List<MpsPersonalInfo> mpInfo;

  const ArtistWidgetSearch({Key key, this.mpInfo, this.position, this.responseString}) : super(key: key);

  @override
  Widget build(BuildContext context) {

      final screen = MediaQuery.of(context).size;

      List<Widget> ListMyWidgets(){

      List<Widget> mpsHorizontal = new List();

      for (var i = 0; i < mpInfo.length; i++){

        if (i < 4){

            Widget specificMp = Container(child: Container(width: screen.width * 0.23, child: Column(children: <Widget>[
            GestureDetector(
              onTap: () {

                //Navigator.push(context, MaterialPageRoute(builder: (context) => ShowDesireArtist(mpInfo[i]), ));

              },
              child: AnimatedOpacity(opacity: 1.0, duration: Duration(seconds: 1), child: Container(
                width: screen.width * 0.15, height: screen.width * 0.15,
                decoration: new BoxDecoration(image: new DecorationImage(
                    image: new NetworkImage(mpInfo[i].profile_photos.thumbnail), fit: BoxFit.fill), borderRadius: new BorderRadius.circular(50.0)),)),),
                Text(mpInfo[i].name, textAlign: TextAlign.center, maxLines: 2, style: TextStyle(fontSize: screen.height * 0.015),), ],),));

          mpsHorizontal.add(specificMp);
        }

      }


      return mpsHorizontal;

    }

    return Column(children: <Widget>[

      Container(margin: EdgeInsets.fromLTRB(screen.width * 0.02, screen.height * 0.01, screen.width * 0.02, 0.0),child: Row(children: <Widget>[
        Expanded(child: Container(margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[Text(mpInfo[position].state, style: TextStyle(fontSize: screen.height * 0.017, fontWeight: FontWeight.bold),)],))),
        Expanded(child: Container(margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(

              onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => ViewMore(responseString)), );},
              child: Text("More", style: TextStyle(fontSize: screen.height * 0.017, fontWeight: FontWeight.bold),),


            )],))),
      ],)
      ),
      Container(margin: EdgeInsets.fromLTRB(screen.width * 0.005, screen.height * 0.02, screen.width * 0.0, screen.width * 0.005),child: Row(mainAxisAlignment: MainAxisAlignment.start,
          children: ListMyWidgets()

      )
      )



    ],
    );
  }
}
