import 'dart:async';
import 'dart:collection';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:suara/Models.dart';
import 'package:suara/ProfileScreen.dart';
import 'Reports.dart';
import 'Mutators.dart';
import 'ApiHelper.dart';
import 'dart:convert';
import 'SearchScreen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';


class MyFirstScreen extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        title: "Suara",
        home: SafeArea(top: true, child: ViewPager()));
  }
}

class ViewPager extends StatefulWidget
{
  bool isTab1Select = true;
  bool isTab2Select = false;
  bool isTab3Select = false;
  bool isTab4Select = false;

  @override
  ViewPagerState createState() => new ViewPagerState();
}

class ViewPagerState extends State<ViewPager> {

  int tabSelected = 0;
  PageController _pageController;
  bool isTab1Selected = false;
  bool isTab2Selected = false;
  bool isTab3Selected = false;
  bool isTab4Selected = false;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();

  }

  GlobalKey globalKey = new GlobalKey(debugLabel: 'btm_app_bar');

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Center(child: new PageView(
        physics:new NeverScrollableScrollPhysics(),
        children: <Widget>[

          new Center(child: new Home()),
          new Center(child: new MainReports()),
          new Center(child: new Search()),
          new Container(child: ProfileScreen()),

        ], controller: _pageController,
           onPageChanged: onPageChanged,

      )),

      bottomNavigationBar: BottomNavigationBar(type: BottomNavigationBarType.fixed, items: <BottomNavigationBarItem>[

        BottomNavigationBarItem(icon: new Icon(Icons.home, ), title: new Text("Home", )),
        BottomNavigationBarItem(icon: new Icon(FontAwesomeIcons.teamspeak, ), title: new Text("Report", )),
        BottomNavigationBarItem(icon: new Icon(FontAwesomeIcons.search, ), title: new Text("Search", )),
        BottomNavigationBarItem(icon: new Icon(FontAwesomeIcons.user, ), title: new Text("Profile", )),],
        currentIndex: tabSelected,
        onTap: navigationTapped,
        fixedColor: Colors.redAccent,

      ),

    );

  }

  void navigationTapped(int index) {
    _pageController.animateToPage(
        index,
        duration: const Duration(milliseconds: 300),
        curve: Curves.ease
    );
  }

  void onPageChanged(int value) {
    setState(() {
      this.tabSelected = value;
    });
  }

}

class TabBottomLayout extends State
{

  @override
  Widget build(BuildContext context) {


  }

}

final ScrollController controller = ScrollController();


class Home extends StatefulWidget{
  bool runOnce = true;
  int page = 1;
  HomeState createState() => HomeState();
  var mpsDynamic;
  List<dynamic> mylist;
  int LastTotalCounts = 0;
  int CurrentTotalCounts = 0;

  Home(){
    mylist = new List();
  }

}

class HomeState extends State<Home>
{

  @override
  void initState() {

    super.initState();

    controller.addListener(() {

      double maxScroll = controller.position.maxScrollExtent;
      double currentScroll = controller.position.pixels;
      double delta = 20.0; // or something else..
      if (maxScroll - currentScroll <= delta) { // whatever you determine here

        if (widget.runOnce){
              widget.runOnce = false;

              HashMap<String, String> ApiParams = new HashMap();
                                      ApiParams.putIfAbsent("access_token", ()=> Config.access_token);
                                      ApiParams.putIfAbsent("page", ()=> (widget.page + 1).toString().trim());

              initApi(Config().API_BASE+ Config().MODULE_FEED, ApiParams, context, true).GetApiFeeds().then((response){

               if (response != '[]'){

                  var decoded = json.decode(response);

                  widget.page = widget.page + 1;

                  widget.mylist.clear();

                  for (int i = 0 ; i < decoded.length; i++){
                      widget.mpsDynamic = Map<String, dynamic>.from(decoded[i]);
                      widget.mylist.add(widget.mpsDynamic);

                  }


                  widget.LastTotalCounts = widget.LastTotalCounts + widget.mylist.length;

                  setState(() {

                        widget.runOnce = true;

                        /*Timer _timer = new Timer(const Duration(seconds: 4), () {

                          controller.

                          //print(widget.LastTotalCounts - widget.mylist.length);

                          controller.animateTo(600.0 * (widget.LastTotalCounts - widget.mylist.length), duration: new Duration(seconds: 1), curve: Curves.ease);
                          //controller.jumpTo((widget.LastTotalCounts - widget.mylist.length).toDouble());


                        });*/

                });
                } else {
                  //widget.page - 1;
                }

              });

        }
      }

    });

  }


  Future _Refresh() async {

      widget.mylist.clear();

      if (widget.page > 1)
          widget.page = widget.page - 1;

      Completer<Null> completer = new Completer<Null>();

      new Future.delayed(new Duration(seconds: 3)).then((_){

        completer.complete();
        setState(() {});

      });
  }

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    final ifnoFollowing = Container(color: Colors.white, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

      Container(width: screen.width * 0.2, height: screen.width * 0.2,
        child: Image.asset("empty_feed.png"),),
      Container(width: screen.width * 0.7, margin: EdgeInsets.all(screen.width * 0.05),
        child: Text("You haven't followed anyone yet. Start following more representatives!", textAlign: TextAlign.center, style: TextStyle(color: Colors.grey),),)

    ],),);

    HashMap<String, String> ApiParams = new HashMap();
                            ApiParams.putIfAbsent("access_token", ()=> Config.access_token);
                            ApiParams.putIfAbsent("page", ()=> widget.page.toString().trim());

    return widget.mylist.length > 0 ?

          new RefreshIndicator(child: ListView.builder(
            controller: controller,
            itemBuilder: (BuildContext context, int Index){

              //print ("HEHE INSIDE "+ widget.mylist[Index].toString());
              if (widget.mylist[Index]["artist"]["genre"] == 'Politics'){

              return Container(margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
              child: new ArtistFeeds(widget.mylist[Index], Index), );

              } else {
                return Container(width: 0.0, height: 0.0);

              }

            },
            itemCount: widget.mylist.length,

          ), onRefresh: _Refresh,)

          : FutureBuilder (future: initApi(Config().API_BASE+Config().MODULE_FEED, ApiParams, context, true).GetApiFeeds(),
                          builder: (BuildContext context, AsyncSnapshot snapshot){

            if (snapshot.data.toString().trim() != "null"){

              if (snapshot.data.toString().trim() != "[]"){

                var decoded = json.decode(snapshot.data);

                widget.LastTotalCounts = widget.LastTotalCounts + decoded.length;

                return new ListView.builder(
                  controller: controller,
                  itemBuilder: (BuildContext context, int Index){

                    widget.runOnce = true;
                    widget.mpsDynamic = Map<String, dynamic>.from(decoded[Index]);
                    widget.mylist.add(widget.mpsDynamic);

                    if ( widget.mpsDynamic["artist"]["genre"] == 'Politics'){

                      return Container(margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                      child: new ArtistFeeds(decoded[Index], Index), );

                    } else {
                      return Container(width: 0.0, height: 0.0);
                    }

                  },
                  itemCount: decoded.length,

                );

              } else {

                return new Container(child: ifnoFollowing);
              }

            } else {

              return new Container(alignment: Alignment.center, child: showCircularIndicator(),);

            }

      });
  }
}

class ArtistFeeds extends StatelessWidget
{

  var mpsFeed = null;
  int Index = 0;

  ArtistFeeds(this.mpsFeed, this.Index);

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    var mpsDynamic = Map<String, dynamic>.from(mpsFeed);

    var comment_btn = GestureDetector(child: Row(children: <Widget>[Icon(Icons.comment, size: 15.0,),
                      mpsDynamic["comment_count"] <= 0 ? Text("Comments", style: new TextStyle(fontSize: 10.0),) :
                        new Text(mpsDynamic["comment_count"].toString() + " Comments", style: new TextStyle(fontSize: 10.0),)]),
                        onTap: (){

                          Navigator.push(context, MaterialPageRoute(builder: (context) => Messages(mpsDynamic["comment_count"],mpsDynamic["id"])));

                        },);


    final circularDp = new Stack(
      children: <Widget>[
        new Container(decoration: new BoxDecoration(image: DecorationImage(image: new NetworkImage(mpsDynamic["artist"]["profile_photos"]["thumbnail"]),
                fit: BoxFit.fill),borderRadius: new BorderRadius.circular(50.0))),
      ],
    );

    bool isBodyPresent = false;

    if (mpsDynamic["body"] != null)
        isBodyPresent = true;

    final introText = new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(mpsDynamic["artist"]["name"],
              style: new TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Bold'),
              textAlign: TextAlign.left ),

          //mpsDynamic["timestamp"]

          new Text("3 months ago on "+ mpsDynamic["type"], style: new TextStyle(fontSize: 10.0, fontFamily: "Regular")),

          new Container(margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
              child: isBodyPresent ? ExpendableText(mpsDynamic["body"], 150, Colors.black) : Text(""))

      ],
      ), margin: new EdgeInsets.fromLTRB(35.0, 5.0, 0.0, 0.0) ,
    );

    final feedImageVideo = mpsDynamic["attachment_type"] == "photo" ? new Container(height: screen.height * 0.4, width: double.infinity, margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
        child: GestureDetector(child: Container(color: Colors.black, child: new Image.network(mpsDynamic["photos"][0]["versions"]["full_resolution"]),),
        onTap: (){

        Navigator.push(context, MaterialPageRoute(builder: (context) => FullViewImage( mpsDynamic["body"], mpsDynamic["photos"][0]["versions"]["full_resolution"])), );

    },)) :

    new Container(height: screen.height * 0.4, width: double.infinity, margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
    child: GestureDetector(child:
    
      Container(width: double.infinity, height: double.infinity, color: Colors.black, child: Stack(children: <Widget>[

        Container(alignment: Alignment.center, child: new Image.network(mpsDynamic["photos"][0]["versions"]["full_resolution"])),
        Container(width: double.infinity, height: double.infinity, color: Colors.black.withOpacity(0.5), ),
        Container(alignment: Alignment.center, child:Image.asset("assets/play.png", width: screen.width * 0.17, height: screen.width * 0.17, ),),

    ],),),



    onTap: (){

      Navigator.push(context, MaterialPageRoute(builder: (context) => FullViewVideo(mpsDynamic["attachment"])));

    },));

    /*



     */


    final bottomBar = new Container(
        child: new Column(children: <Widget>[

          new Container(color: Colors.grey , height: 0.5, margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 10.0),),
          new Container(height: 25.0 ,

              child: Row(children: <Widget>[
                Row(crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0), child: LikeBtn(mpsDynamic["id"].toString().trim(), mpsDynamic["liked"], mpsDynamic["like_count"])),
                      new Container(margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0), child: comment_btn),
                      new Container(margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0), child: new Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Icon(Icons.face, size: 15.0,), Icon(Icons.arrow_drop_down, size: 15.0,)],),),
                      new Container(width: 150.0,child: Align(alignment : Alignment.centerRight, child: Icon(Icons.more_horiz,size: 20.0, color: Colors.grey, )))
                    ]),


                //   Container(child: Icon(Icons.more_horiz))
              ],

              ))],)
    );


    return new Row( mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(width: 350.0,
          child: new Stack(
            children: <Widget>[
              new Container(
                child: new Card(
                    margin: new EdgeInsets.fromLTRB(25.0, 0.0, 0.0, 10.0),
                    color: Colors.white,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        introText, feedImageVideo, bottomBar

                      ],
                    )

                ),
              ),
              new Container( width: 50.0, height: 50.0,
                  child: circularDp

              ),

            ],

          ),
        )
      ],
    );
  }

}


class LikeBtn extends StatefulWidget{

  bool likeit = false;
  bool isLikeAlready;
  bool likedNow = false;


  int totalLikes;

  String feedId = "";

  LikeBtn(this.feedId, this.isLikeAlready, this.totalLikes);

  LikeState createState() => new LikeState();

}

class LikeState extends State<LikeBtn>{


  @override
  Widget build(BuildContext context) {

    HashMap<String, String> ApiParams = new HashMap();
    ApiParams.putIfAbsent("access_token", ()=> Config.access_token);


    if (widget.likeit){


        if (widget.isLikeAlready){

          initApi(Config().API_BASE + Config().MODULE_POST + widget.feedId + Config().MODULE_LIKES, ApiParams, context, true).DeleteApi().then((response){

            if (responseStatus == StatusOkay){

              setState(() {

                widget.isLikeAlready = false;
                widget.totalLikes = widget.totalLikes - 1;
                widget.likeit = false;

              });

            }

          });


        } else {

          initApi(Config().API_BASE + Config().MODULE_POST + widget.feedId + Config().MODULE_LIKES, ApiParams, context, true).PostApi().then((response){

            if (responseStatus == StatusOkay){

              setState(() {

                widget.isLikeAlready = true;
                widget.totalLikes = widget.totalLikes + 1;
                widget.likeit = false;

              });

            }

          });


        }

    }

    return GestureDetector(child: Row(children: <Widget>[
            widget.isLikeAlready ? Icon(Icons.favorite, size: 15.0, color: Colors.red,): Icon(Icons.favorite_border, size: 15.0,),
            Text(widget.totalLikes <= 0 ? "Like" : widget.totalLikes.toString()+" Likes" , style: new TextStyle(fontSize: 10.0),)
            ],), onTap: (){

            setState(() {

              widget.likeit = true;

      });

    },);

  }

}


class ExpendableText extends StatefulWidget{

  bool isExpanded = false;
  final String text;
  final int limitChar;
  final Color color;

  ExpendableText(this.text, this.limitChar, this.color);

  ExpendableState createState() => new ExpendableState();

}

class ExpendableState extends State<ExpendableText> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > widget.limitChar) {
      firstHalf = widget.text.substring(0, widget.limitChar);
      secondHalf = widget.text.substring(widget.limitChar, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.symmetric(vertical: 10.0),
      child: secondHalf.isEmpty
          ? new Text(firstHalf, style: TextStyle(color: widget.color, fontFamily: "Regular", fontSize: 12))
          : new Column(
        children: <Widget>[
          new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf), style: TextStyle(color: widget.color, fontFamily: "Regular", fontSize: 12),),
          new InkWell(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Text(
                  flag ? "Show more" : "Show less",
                  style: new TextStyle(color: Colors.red),
                ),
              ],
            ),
            onTap: () {
              setState(() {
                flag = !flag;
              });
            },
          ),
        ],
      ),
    );
  }
}


class Messages extends StatefulWidget {

  int totalComment = 0;
  int postid = 0;


  Messages(this.totalComment, this.postid);

  MessagesState createState() => new MessagesState();

}

class MessagesState extends State<Messages>{

  final commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    var screen = MediaQuery.of(context).size;

    var noComments = Container(child: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(width: screen.width * 0.2, height: screen.width * 0.2,
          child: Image.asset("no_message.jpg"),),
          Container(margin: EdgeInsets.all(screen.width * 0.05), child: Text("No Comments yet!"),)
        ],)));


      var commentField = Container(decoration: BoxDecoration(border: new Border(top: BorderSide(color: Colors.grey, width: 1.0))), height: screen.height * 0.1, child: Container(margin: EdgeInsets.all(screen.width * 0.01),

        child: Container(margin: EdgeInsets.all(10.0), child: Row(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Expanded(child: Container(child:
                    TextField(controller: commentController, decoration: InputDecoration(border: InputBorder.none, hintText: 'Add a comment'),))),
                Container(width: screen.width * 0.12, height: screen.width * 0.12,

                    child: RaisedButton(child: Icon(Icons.send, color: Colors.white,),
                    elevation: 4.0,
                    splashColor: Colors.grey,
                    color: Colors.redAccent,
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(screen.width * 0.2)),
                    onPressed: (){

                      if (!commentController.text.isEmpty){

                        postMessage();

                      }else {

                        Scaffold.of(context).showSnackBar(new SnackBar(
                          content: new Text("Your comment is empty!"),
                        ));

                      }

                    },
                    ))],),),

      ),);



      //var messagesList = Container(ch)

      int postid = widget.postid;


      HashMap<String, String> ApiParams = new HashMap();
      ApiParams.putIfAbsent("access_token", ()=> Config.access_token);

      return Scaffold(
        appBar: AppBar(iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white, title: Text("Comments", style: TextStyle(color: Colors.black),),),
        body: Container(child: Stack(children: <Widget>[

            FutureBuilder(future: initApi(Config().API_BASE + Config().MODULE_POST + "$postid/comments", ApiParams, context, true).GetApi(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {


                if (responseStatus == StatusOkay){

                  var jsons = json.decode(snapshot.data.toString().trim());

                    var comment = List<dynamic>.from(jsons["comments"]);

                    var count = jsons["count"];

                    return count > 0 ? Container(width: screen.width, height: screen.height, child: new ListView.builder(
                      itemBuilder: (BuildContext context, int Index){

                        return Container(margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 5.0),
                        child: new CommentsList(comment[Index]["from"]["name"], comment[Index]["from"]["profile_photos"]["thumbnail"], comment[Index]["body"], comment[Index]["timestamp"]));},

                      itemCount: count,

                    )): noComments;



                } else {

                    noComments;
                }

              },

            ), Container(child: Align(alignment: Alignment.bottomCenter,child: commentField)),

        ],),


            ),);

        //child: totalComment == 0 ? noComments :  noComments



  }


  /*if (jsons["count"] > 0){

  }

} else {

Container(child: noComments);





}*/

  void postMessage(){

    HashMap<String, String> ApiParams = new HashMap();
                            ApiParams.putIfAbsent("access_token", ()=> Config.access_token);
                            ApiParams.putIfAbsent("body", ()=> commentController.text.toString().trim());


    int postid = widget.postid;

    initApi(Config().API_BASE + Config().MODULE_POST + "$postid/comments", ApiParams, context, true).PostApi().then((response){

      if (responseStatus == StatusOkay){

        setState(() {

        });
      }

    });

  }

}


class CommentsList extends StatelessWidget{

  String name = "";
  String dp = "";
  String body = "";
  String date = "";

  CommentsList(this.name, this.dp, this.body, this.date);

  @override
  Widget build(BuildContext context) {


    final screen = MediaQuery.of(context).size;

    final circularDp = new Container(
        width: screen.width * 0.15,
        height: screen.width * 0.15, decoration: new BoxDecoration(image: DecorationImage(image: new NetworkImage(dp),
        fit: BoxFit.fill),borderRadius: new BorderRadius.circular(50.0)));

    return Container(margin: EdgeInsets.only(top: screen.height * 0.02), width:  screen.width,  child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
          circularDp,
          Expanded(child: Container(margin: EdgeInsets.only(left: screen.width * 0.05),child: Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text(name, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
              Container(margin: EdgeInsets.only(top: 5.0), child: Text(body, style: TextStyle(color: Colors.blueGrey, fontSize: screen.height * 0.02),)),
              Container(margin: EdgeInsets.only(top: 5.0), child: Text("Few moments ago", style: TextStyle(color: Colors.grey, fontSize: screen.height * 0.01),),)

            ],
          )))

    ],),);

  }

}

/*

  Column(mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Container(child: Column(children: <Widget>[
                Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Colors.red
                  ),
                  child: Center(child: InkWell(child: Icon(Icons.camera_alt, color: Colors.white, size: screen.width * 0.12,),onTap: () {

                    openCamera(context);

                  },),),
                ),
                Text("Take a Photo", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
              ],)
              ),
              Container(child: Column(children: <Widget>[
                Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Colors.orange
                  ),
                  child: Center(child: InkWell(child: Icon(Icons.picture_in_picture, color: Colors.white, size: screen.width * 0.12,),),),
                ),
                Text("Choose from Gallery", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
              ],)
              ),


            ],)),

            Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

              Container(child: Column(children: <Widget>[
                Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Colors.purple
                  ),
                  child: Center(child: InkWell(child: Icon(Icons.videocam, color: Colors.white, size: screen.width * 0.12,),),),
                ),
                Text("Record a video", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
              ],)
              ),
              Container(child: Column(children: <Widget>[
                Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Colors.blueAccent
                  ),
                  child: Center(child: InkWell(child: Icon(Icons.live_tv, color: Colors.white, size: screen.width * 0.12,),),),
                ),
                Text("Start live stream", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
              ],)
              ),

            ],)) ,
            Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

              IconButton(iconSize: screen.width * 0.08, icon: Icon(Icons.close, color: Colors.white),
                onPressed: (){

                  Navigator.pop(context);

                },)

            ],),)
          ],

        )


 */

class FullViewImage extends StatelessWidget{

  String body;
  String imagePath;


  FullViewImage(this.body, this.imagePath);

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Scaffold(appBar: AppBar(backgroundColor: Colors.black, iconTheme: IconThemeData(color: Colors.white,)),
      body: Container(color: Colors.black, child:

        Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[

          Container(alignment: Alignment.center, width: screen.width, height: screen.height * 0.4, child: Image.network(imagePath)),

          Container(child: Container(alignment: Alignment.bottomCenter, height: screen.height * 0.25, child: new SingleChildScrollView(
              physics: null,
            child: new ConstrainedBox(
              constraints: new BoxConstraints(),
              child: Container(child: body != null ? ExpendableText(body, 100, Colors.white) : Text("")),))),),

          Container(height: screen.height * 0.1,)

        ]),));


  }

}


class FullViewVideo extends StatefulWidget {

  String VideoPath = "";

  FullViewVideo(this.VideoPath);


  @override
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<FullViewVideo> {
  VideoPlayerController _controller;
  bool _isPlaying = false;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.VideoPath,)
      ..addListener(() {
        final bool isPlaying = _controller.value.isPlaying;
        if (isPlaying != _isPlaying) {
          setState(() {
            _isPlaying = isPlaying;
          });
        }
      })
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      })
      ..play();
  }

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(backgroundColor: Colors.black, iconTheme: IconThemeData(color: Colors.white,)),
        body: Container(color: Colors.black, child: Stack(children: <Widget>[
          Center(
            child: _controller.value.initialized
                ? AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: VideoPlayer(_controller),
            )
                : Container(),
          ),

          Container(margin: EdgeInsets.all(screen.width * 0.05),
            alignment: Alignment.bottomCenter, child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            FloatingActionButton(
              backgroundColor: Colors.red,
              onPressed: _controller.value.isPlaying
                  ? _controller.pause
                  : _controller.play,

              child: Icon(
                _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
              ),
            ),

          ],),)

        ],),),

    );
  }
}