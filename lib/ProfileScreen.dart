
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:suara/Main.dart';
import 'package:suara/Models.dart';
import 'package:suara/Mutators.dart';
import 'package:suara/ViewMore.dart';
import 'ApiHelper.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';



class ProfileScreen extends StatefulWidget{

   bool firstRun = false;
   String access_token = "";
   PersonInfo myInfo;

   ProfileScreenState createState() => new ProfileScreenState();

}

class ProfileScreenState extends State<ProfileScreen>{


  @override
  Widget build(BuildContext context) {


    if (widget.access_token == "")
    SuaraStorage().getString(Config().LoginDetails).then((value){

      setState(() {

        final jsonResponse = json.decode(value);
        widget.myInfo = new PersonInfo.fromJson(jsonResponse);
        widget.access_token = widget.myInfo.access_token;


      });

    });


    int mpCount = 0;

    final screen = MediaQuery.of(context).size;

    final ifnoFollowing = Container(color: Colors.white, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

      Container(width: screen.width * 0.2, height: screen.width * 0.2,
        child: Image.asset("empty_feed.png"),),
      Container(width: screen.width * 0.7, margin: EdgeInsets.all(screen.width * 0.05),
        child: Text("You haven't followed anyone yet. Start following more representatives!", textAlign: TextAlign.center, style: TextStyle(color: Colors.grey),),)
      
    ],),);

    HashMap<String, String> ApiParams = new HashMap();
                            ApiParams.putIfAbsent("access_token", ()=> widget.access_token == "" ? " " : widget.access_token);

    return new Container(color: Colors.white, child: Column(children: <Widget>[

      Container(height: screen.height * 0.08, color: Colors.white, child: Row(mainAxisAlignment: MainAxisAlignment.end, 
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Container(margin: EdgeInsets.all(screen.width * 0.01), child: GestureDetector(onTap: (){

          Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));

        },child: Icon(Icons.settings, color: Colors.blueGrey, size: screen.width * 0.08,),))],),),

      Container(margin: EdgeInsets.only(top: screen.height * 0.1), child:

      Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

        Container(width:  screen.width * 0.3, height: screen.width * 0.3,
          decoration: new BoxDecoration(border: Border.all(width: 4.0, color: Colors.blue), borderRadius: new BorderRadius.circular(100.0)),

          child: Stack(children: <Widget>[

            Center(child: GestureDetector(child: Container(width: screen.width * 0.26, height: screen.width * 0.26, decoration: BoxDecoration(
                borderRadius: new BorderRadius.circular(100.0),
                image: DecorationImage(
                    image: widget.myInfo == null  || widget.myInfo.profile_photos.thumbnail.contains("cloudfront") ? AssetImage("assets/me_icon.png") : new NetworkImage(widget.myInfo.profile_photos.thumbnail),
                    fit: BoxFit.fill)
            ),),onTap: () async{

              File img = await ImagePicker.pickImage(source: ImageSource.gallery);

              var uri = Uri.parse("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos?access_token="+widget.access_token);
              var request = new MultipartRequest("POST", uri);

              var multipartFile = await MultipartFile.fromPath("file", img.path);
              String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

              request.headers.putIfAbsent("Accept", () => "application/json");
              request.headers.putIfAbsent("Authorization",()=> auth);

              //request.headers.putIfAbsent("access_token", () => widget.access_token);
              request.files.add(multipartFile);


              StreamedResponse response = await request.send();
              response.stream.transform(utf8.decoder).listen((value) {


                print(value);


              });




            },)),

            Container(child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[Container(width: screen.width * 0.07 , height: screen.width * 0.07, decoration: BoxDecoration( border: Border.all(color: Colors.blue, width: 1.0),
                  borderRadius: new BorderRadius.circular(100.0), color: Colors.white), child:
              Container(child: Image.asset("assets/s_icon.png",scale: screen.height * 0.005, color: Colors.blue,),),)],)
              ,)


          ],),
        ),

        Container(margin: EdgeInsets.only(top: screen.height * 0.01),child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Text(widget.myInfo == null? "": widget.myInfo.name, style: TextStyle(color: Colors.black, fontSize: screen.height * 0.03),),
          //GestureDetector(child: Icon(Icons.mode_edit),)
        ],)),

        Container(height: 50.0, margin: EdgeInsets.only(top: screen.height * 0.05), child:
        Stack(children: <Widget>[

          Center(child: Container(height: 1.0, color: Colors.grey,),),
          Center(child: Container(decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(200.0), border: Border.all(color: Colors.grey, width: 1.0)),
              width: screen.width * 0.3, height: screen.height * 0.05,
              child: Center(child: Container(child: Text("Following", style: TextStyle(color: Colors.black54, fontSize: screen.height * 0.02),)))),),

        ],),)

      ]


      ),),

      //widget.fetchLocalValue == true ?
      Expanded(child: Container(color: Colors.white,

        child: FutureBuilder(future: initApi(Config().API_BASE+ Config().MODULE_ME_FOLLOWING, ApiParams, context, false).GetApi(),
            builder: (BuildContext context, AsyncSnapshot response){

              if (widget.access_token != ""){

                widget.firstRun = false;

                if (response.hasData && widget.myInfo != null){

                  if (response.data != "[]"){

                    var decoded = json.decode(response.data);

                    MeFollowing myFollowers = MeFollowing.fromJson(decoded);

                    for (int i = 0; i < myFollowers.count ; i++){

                      if (myFollowers.followings[i].genre == "Politics")
                          mpCount++;

                    }

                    if (mpCount <= 0){

                      return ifnoFollowing;

                    }else {

                      return ListView.builder(itemBuilder: (BuildContext cnt,int index){

                        return myFollowers.followings[index].genre == "Politics" ? Container(margin: EdgeInsets.only(top: screen.height* 0.02),height: screen.height * 0.15 ,
                          child: AdapterList(mpsList: myFollowers.followings, position: index,), )  : Container(width: 0.0, height: 0.0,);

                      },itemCount: mpCount);

                    }


                  } else {

                    return ifnoFollowing;
                  }

                } else {

                  return ifnoFollowing;

                }

              } else {

                return Container(child: showCircularIndicator());

              }


            }),
      )),
    ],
    ),
    );


  }
}


class Settings extends StatelessWidget{



  @override
  Widget build(BuildContext context) {

    var screen = MediaQuery.of(context).size;
    
    return Scaffold(appBar: AppBar(iconTheme: IconThemeData(
      color: Colors.black, //change your color here
      ),backgroundColor: Colors.white, title: Text("Settings", style: TextStyle(color: Colors.black, fontFamily: "Regular", fontSize: 18.0),),),

        body: Container(child: Column(
      children: <Widget>[
        Container(margin: EdgeInsets.only(top: screen.height * 0.06),),
        InkWell(child: Container(decoration: BoxDecoration( color: Colors.white, border: Border.all(color: Colors.grey, width: 0.08)), height: screen.height * 0.07, width: screen.width, child: Container(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(margin: EdgeInsets.only(left: 20.0 ),child: Text("Terms of Service", style: TextStyle(color: Colors.blueGrey),)),
            Expanded(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[Icon(Icons.keyboard_arrow_right, color: Colors.blueGrey,)],),),)],)
          ,),), onTap: (){

          Navigator.push(context, MaterialPageRoute(builder: (context) => WebViewScaffold("Terms of Service", Config().tosUrl)));

        },),
        InkWell(child: Container(decoration: BoxDecoration( color: Colors.white, border: Border.all(color: Colors.grey, width: 0.08)), height: screen.height * 0.07, width: screen.width, child: Container(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(margin: EdgeInsets.only(left: 20.0 ),child: Text("Privacy Policy", style: TextStyle(color: Colors.blueGrey),)),
              Expanded(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[Icon(Icons.keyboard_arrow_right, color: Colors.blueGrey,)],),),)],)
          ,),), onTap: (){

          Navigator.push(context, MaterialPageRoute(builder: (context) => WebViewScaffold("Privacy Policy", Config().privacyUrl)));


        },),
        InkWell(child: Container(decoration: BoxDecoration( color: Colors.white, border: Border.all(color: Colors.grey, width: 0.08)), height: screen.height * 0.07, width: screen.width, child: Container(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(margin: EdgeInsets.only(left: 20.0 ),child: Text("Frequently Asked Questions", style: TextStyle(color: Colors.blueGrey),)),
              Expanded(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[Icon(Icons.keyboard_arrow_right, color: Colors.blueGrey,)],),),)],)
          ,),), onTap: (){

          Navigator.push(context, MaterialPageRoute(builder: (context) => WebViewScaffold("Frequently Asked Questions", Config().faqUrl)));


        },),
        Container(margin: EdgeInsets.only(top: screen.height * 0.06),),

        Container(color: Colors.white, height: screen.height * 0.07, width: screen.width, child: InkWell(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(margin: EdgeInsets.only(left: 20.0 ), child: Text("Logout", style: TextStyle(color: Colors.blueGrey),)),
              Expanded(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[Icon(Icons.keyboard_arrow_right, color: Colors.blueGrey,)],),),)],)
          ,),onTap: (){



          SuaraStorage().getString(Config().LoginDetails).then((response){

            final jsonResponse = json.decode(response);
            PersonInfo myInfo = new PersonInfo.fromJson(jsonResponse);

            HashMap<String, String> ApiParams = new HashMap();
            ApiParams.putIfAbsent("access_token",  () => myInfo.access_token);

            initApi(Config().API_BASE + Config().API_LOGOUT, ApiParams, context, true).PostApi().then((responseString){

              if (responseStatus == StatusOkay && responseString != ""){

                var decoded = json.decode(responseString);
                Logout logout =  Logout.fromJson(decoded);

                if (logout.logged_out){

                  SuaraStorage().saveString(Config().LoginDetails, "");
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => StartApp()));

                }
              }

            });



          });


        },), ),

      ],
    ),));

  }

}


class WebViewScaffold extends StatelessWidget{

  String title;
  String url;

  WebViewScaffold(this.title, this.url);

  @override
  Widget build(BuildContext context) {



    return new WebviewScaffold(url: url, appBar: AppBar(title: Text(title, style: TextStyle(fontFamily: "Regular"),),), withZoom: true,);

  }

}
























