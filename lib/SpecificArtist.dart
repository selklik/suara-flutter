import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:suara/ApiHelper.dart';
import 'package:suara/Mutators.dart';
import 'Models.dart';



class ShowDesireArtist extends StatelessWidget {


  MpsPersonalInfo mpsPersonalInfo;

  ShowDesireArtist(this.mpsPersonalInfo);


  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Scaffold(body: SafeArea(top: true, child: new CustomScrollView(
                       slivers: <Widget>[
                         new SliverAppBar(
                           backgroundColor: Colors.black,
                           expandedHeight: 230.0,
                           floating: false,
                           pinned: true,
                           actions: <Widget>[
                             Container(margin: EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),child: IconButton(icon: Icon(Icons.settings), color: Colors.white,
                               onPressed: (){

                                 Navigator.of(context).push<Widget>(PageRouteBuilder<Widget>(
                                     opaque: true,
                                     transitionDuration: Duration(days: 1),
                                     pageBuilder: (BuildContext context, _, __) {
                                       return Container(
                                         child: FeatureScreen(),
                                       );
                                     }));


                               },))
                           ],
                           flexibleSpace: new FlexibleSpaceBar(
                             centerTitle: true,
                             //title: Container( widthchild: Text("Dr Mahathir Mohammad", style: TextStyle(fontSize: 16.0),), margin: EdgeInsets.all(70.0),),
                             background: Stack(
                               children: <Widget>[
                                 Container(decoration: BoxDecoration(
                                   image: DecorationImage(image: NetworkImage(mpsPersonalInfo.profile_photos.full_resolution,),
                                     fit: BoxFit.cover,

                                   )
                                 ), child:

                                 Container( child: new BackdropFilter(
                                   filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                   child: new Container(
                                     decoration: new BoxDecoration(color: Colors.black.withOpacity(0.5)),
                                   ),
                                 ),

                                 ),),


                                Column(children: <Widget>[
                                   Container( margin: EdgeInsets.fromLTRB(10.0, 65.0, 10.0, 10.0), child:
                                   Row(children: <Widget>[
                                     Container( height: 100.0, width: 100.0,
                                         decoration: BoxDecoration(image: new DecorationImage(
                                             image: new NetworkImage(mpsPersonalInfo.profile_photos.thumbnail), fit: BoxFit.fill), borderRadius: new BorderRadius.circular(50.0))
                                     ),
                                     Expanded( child: Container(margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),child: Column(mainAxisAlignment: MainAxisAlignment.center,
                                       children: <Widget>[

                                         Text(mpsPersonalInfo.name, style: TextStyle(fontSize: 22.0, color: Colors.white),),
                                         Row(children: <Widget>[
                                           Container(margin: EdgeInsets.fromLTRB(0.0, 3.0, 5.0, 0.0), child: Text("Malaysia", style: TextStyle(fontSize: 13.0, color: Colors.grey),)),
                                           Container(margin: EdgeInsets.fromLTRB(5.0, 3.0, 0.0, 0.0), child: Text(mpsPersonalInfo.followers_count.toString()+" Followers", style: TextStyle(fontSize: 13.0, color: Colors.grey),)),

                                         ],),

                                       ],

                                     ))),
                                   ],)),
                                   // Bottom Screen
                                   Container(margin: EdgeInsets.fromLTRB(40.0, 0.0, 0.0, 0.0), child: Row(children: <Widget>[

                                     Expanded(child:
                                      //-- Icons Facebook, Twitter, Instagram
                                       Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                           Container(margin: EdgeInsets.fromLTRB(0.0, 6.0, 5.0, 0.0),
                                             child: Icon(FontAwesomeIcons.facebook, color: Colors.white, size: 20.0,),),
                                           Container(margin: EdgeInsets.fromLTRB(6.0, 6.0, 5.0, 0.0),
                                             child: Icon(FontAwesomeIcons.twitterSquare, color: Colors.white, size: 20.0,),),
                                           Container(margin: EdgeInsets.fromLTRB(6.0, 6.0, 5.0, 0.0),
                                             child: Icon(FontAwesomeIcons.instagram, color: Colors.white, size: 20.0,),),


                                       ],)),

                                     Container(margin: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                                         child: Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[

                                           Follow_btn(),

                                       ],)),


                                   ],),
                                   )
                                 ],)

                               ],
                             ),
                           )
                         ),
                         new SliverList(delegate: new SliverChildBuilderDelegate((context, index) => new ListTile(
                           title: new Text("List Item $index")),
                         ))
                       ],
                     )),

    );

  }

}


class FeatureScreen extends StatefulWidget{

  FeatureScreen_State createState() => new FeatureScreen_State();


}

class FeatureScreen_State extends State<FeatureScreen>{

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return MaterialApp(home: Scaffold(backgroundColor: Colors.transparent,body: Stack(children: <Widget>[
    new BackdropFilter(
      filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: new Container(
        decoration: new BoxDecoration(
          color: Colors.grey.withOpacity(0.5)
          ),

      ),
    ),

      Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(child: Column(children: <Widget>[
              Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.red
                ),
                child: Center(child: InkWell(child: Icon(Icons.camera_alt, color: Colors.white, size: screen.width * 0.12,),onTap: () {

                  openCamera(context);

                },),),
              ),
              Text("Take a Photo", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
            ],)
            ),
            Container(child: Column(children: <Widget>[
              Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.orange
                ),
                child: Center(child: InkWell(child: Icon(Icons.picture_in_picture, color: Colors.white, size: screen.width * 0.12,),),),
              ),
              Text("Choose from Gallery", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
            ],)
            ),


          ],)),

          Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

            Container(child: Column(children: <Widget>[
              Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.purple
                ),
                child: Center(child: InkWell(child: Icon(Icons.videocam, color: Colors.white, size: screen.width * 0.12,),),),
              ),
              Text("Record a video", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
            ],)
            ),
            Container(child: Column(children: <Widget>[
              Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    color: Colors.blueAccent
                ),
                child: Center(child: InkWell(child: Icon(Icons.live_tv, color: Colors.white, size: screen.width * 0.12,),),),
              ),
              Text("Start live stream", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
            ],)
            ),

          ],)) ,
          Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

            IconButton(iconSize: screen.width * 0.08, icon: Icon(Icons.close, color: Colors.white),
              onPressed: (){

                Navigator.pop(context);

              },)

          ],),)
        ],

      ),)




      /*Column(crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(height: screen.height * 0.5, decoration: BoxDecoration(
          borderRadius: new BorderRadiusDirectional.only(
              bottomEnd: Radius.circular(20.0),
              bottomStart: Radius.circular(20.0)),

          color: Colors.grey

        ),),
        RaisedButton(onPressed: () {Navigator.pop(context);})
      ],
    )*/],)),);
  }
}




class complainBox extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    HashMap<String, String> ApiParams = new HashMap();

    return FutureBuilder(future: initApi(Config().complainList, ApiParams, context, false).GetApi(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {

            if (responseStatus == StatusOkay){

              if (snapshot.data != "[]"){


                List<Widget> ListMyWidgets(){

                }

              } else {


              }

            } else {

              Container(child: CircularProgressIndicator(),);

            }

        });


  }

}

class Follow_btn extends StatefulWidget {

  FollowBtnState createState() => FollowBtnState();

}

class FollowBtnState extends State<Follow_btn> {

  bool follow = false;

  @override
  Widget build(BuildContext context) {

    return follow ?
    InkWell(onTap: (){ setState(() {follow = false;});},

    child: Container(height: 30.0, decoration: new BoxDecoration(
        color: Colors.transparent,
        border: new Border.all(color: Colors.white, width: 0.5),
        borderRadius: new BorderRadius.circular(5.0),),

          child: Row(children: <Widget>[Icon(Icons.check, size: 16.0, color: Colors.white,),
          Text("Following", style: TextStyle(color: Colors.white),)],

          )),
      )
      :
      InkWell(onTap: (){ setState(() {

        follow = true;

      });},

      child: Container(height: 30.0, decoration: new BoxDecoration(
        color: Colors.transparent,
        border: new Border.all(color: Colors.white, width: 0.5),
        borderRadius: new BorderRadius.circular(5.0),),

          child: Row(children: <Widget>[Icon(FontAwesomeIcons.cut, size: 16.0, color: Colors.red,),
          Text("Following", style: TextStyle(color: Colors.white),)],

          )),
    );
  }
}






/*Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,
children: <Widget>[
Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
Container(child: Column(children: <Widget>[
Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(50.0),
color: Colors.red
),
child: Center(child: InkWell(child: Icon(Icons.camera_alt, color: Colors.white, size: screen.width * 0.12,),onTap: () {

openCamera(context);

},),),
),
Text("Take a Photo", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
],)
),
Container(child: Column(children: <Widget>[
Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(50.0),
color: Colors.orange
),
child: Center(child: InkWell(child: Icon(Icons.picture_in_picture, color: Colors.white, size: screen.width * 0.12,),),),
),
Text("Choose from Gallery", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
],)
),


],)),

Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

Container(child: Column(children: <Widget>[
Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(50.0),
color: Colors.purple
),
child: Center(child: InkWell(child: Icon(Icons.videocam, color: Colors.white, size: screen.width * 0.12,),),),
),
Text("Record a video", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
],)
),
Container(child: Column(children: <Widget>[
Container(margin: EdgeInsets.all(20.0), width: screen.width * 0.25, height: screen.width * 0.25,
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(50.0),
color: Colors.blueAccent
),
child: Center(child: InkWell(child: Icon(Icons.live_tv, color: Colors.white, size: screen.width * 0.12,),),),
),
Text("Start live stream", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
],)
),

],)) ,
Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

IconButton(iconSize: screen.width * 0.08, icon: Icon(Icons.close, color: Colors.white),
onPressed: (){

Navigator.pop(context);

},)

],),)
],

),),*/


List<CameraDescription> cameras;

Future<Null> openCamera(BuildContext context) async {
  cameras = await availableCameras();
  //runApp(new MaterialApp(home: CameraApp()));

  Navigator.of(context).push<Widget>(PageRouteBuilder<Widget>(
      opaque: true,
      transitionDuration: Duration(days: 1),
      pageBuilder: (BuildContext context, _, __) {
        return Container(
          child: CameraApp(),
        );
      }));
}

class CameraApp extends StatefulWidget {
  @override
  _CameraAppState createState() => new _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {

  CameraController controller;

  bool takePicture = false;

  @override
  void initState() {


    super.initState();
    controller = new CameraController(cameras[0], ResolutionPreset.high);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _saveImage() async {

    Directory appDocDir = await getApplicationDocumentsDirectory().whenComplete((){

    });

    String appDocPath = appDocDir.path;

    print(appDocDir);

    controller.takePicture(appDocPath+"/myImage.png");

  }

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;


    if (takePicture) {

      _saveImage();
      takePicture = false;

    } else {


    }

    if (!controller.value.isInitialized) {
      return new Column();
    }

    return new AspectRatio(
        aspectRatio:
        controller.value.aspectRatio,
        child: Scaffold(body: new Stack(children: <Widget>[
          CameraPreview(controller),
          Column (children: <Widget>[
            Container(height: screen.height* 0.70, color: Colors.transparent,
              child: Column(children: <Widget>[
                Container(height: screen.height* 0.2, child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[InkWell(child: Icon(Icons.cancel, color: Colors.white, size: screen.width * 0.1,),onTap: (){

                    Navigator.pop(context);

                  },)],
                ),)
            ],),),

                Container(height: screen.height* 0.23, width: double.infinity,
                  child: Stack(children: <Widget>[new BackdropFilter(
                  filter: new ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: new Container(
                      decoration: new BoxDecoration(
                        color: Colors.grey.shade200.withOpacity(0.05)
                    ),

                    ),

                ),

                Center(child: Container(width: screen.width * 0.2, height: screen.width * 0.2,
                  child: Container( child: InkWell(
                    child: Container(decoration: BoxDecoration(color: Colors.red,
                        borderRadius: BorderRadius.circular(screen.width * 0.5)),), onTap: (){

                        setState(() {

                          takePicture = true;

                        });

                },),))),


              ],)

            ,),
          Container(height: screen.height* 0.07, color: Colors.black,
              child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

                    Text("Powered by", style: TextStyle(color: Colors.white),),
                    Container(margin: EdgeInsets.only(left: screen.width * 0.005),height: screen.height*0.02, child: Image.asset("selklik_icon.png")),

              ],),),

      ]),],),));
  }

}




















