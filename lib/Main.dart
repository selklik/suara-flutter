import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:suara/LoginPage.dart';
import 'Mutators.dart';
import 'ApiHelper.dart';
import 'MainMenu.dart';
import 'Models.dart';

import 'package:http/http.dart' as http;

import 'package:image_picker/image_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:dio/dio.dart';

void main() {

  /*SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {*/


        runApp(new MaterialApp(debugShowCheckedModeBanner: false, home: Scaffold(resizeToAvoidBottomPadding: false, primary: false, body: SplashScreen())));

    //runApp(new MaterialApp(home: Scaffold(resizeToAvoidBottomPadding: false, body: )));

  //});

}


class ShowLoginScreen extends StatefulWidget{


  @override
  AuthenticationScreen createState() =>  new AuthenticationScreen();

}


class SplashScreen extends StatelessWidget{


  @override
  Widget build(BuildContext context) {


    final screen = MediaQuery.of(context).size;

    final splashScreen = Container(color: Colors.white, child: Center(
      child: Container(
          width: screen.width * 0.25,
          height: screen.width * 0.25,
          child: Image.asset("assets/suara_logo.png")),)

    );

    return FutureBuilder(future: SuaraStorage().getString(Config().LoginDetails),
                         builder: (BuildContext context, AsyncSnapshot onValue){

          if (onValue.hasData){

            if (onValue.data != "" && onValue.data != "[]" ){

              SuaraStorage().saveString(Config().LoginDetails, onValue.data);
              final jsonResponse = json.decode(onValue.data);
              PersonInfo myInfo = new PersonInfo.fromJson(jsonResponse);
              Config.access_token = myInfo.access_token;

              return MyFirstScreen();


            } else {

              return LoginPage();

            }

          } else {

            return splashScreen;

          }

    });

  }
}

class StartApp extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return MaterialApp(home: Scaffold(body: LoginPage()),);
  }

}



final passwordFieldController = TextEditingController();
final emailFieldController = TextEditingController();
final nameFieldController = TextEditingController();

class AuthenticationScreen extends State<ShowLoginScreen>{

  Config mConfig;

  @override
  Widget build(BuildContext contexts) {

    final size = MediaQuery.of(contexts).size;
    mConfig = new Config();

    return new Container(
      child: Stack(children: <Widget>[
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("suara_login_bg.png"), fit: BoxFit.fill)),
          child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 0.0, sigmaY: 0.0),
              child: new Container(
                decoration: new BoxDecoration(color: Colors.grey.withOpacity(0.5)),
              )),
        ),

        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.transparent, Colors.grey],
                  stops: [0.0, 1.0],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  tileMode: TileMode.repeated)
          ),

        ),
        Container(child: Column(children: <Widget>[
          Expanded(child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(child:
            Container(width: size.width * 0.23, height:  size.width * 0.23,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("suara_logo.png"),
                      fit: BoxFit.contain)),
            ),),
            Text("Suara", style: TextStyle(color: Colors.white,
                fontSize: 30.0,
                letterSpacing: 7.0,
                fontFamily: 'RobotoMono') ,)
          ],)
          ),
          Container(height: size.height * 0.5, width: double.infinity, child:  Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[ // width: size.width*2, height: size.width*2,child: Icon(Icons.lock_outline),

              Container(width: size.width * 0.8, height: 100.0,  decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.circular(size.width * 0.01)),
                child:  Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

                  //Email Field
                  Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                    Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Icon(Icons.email)),
                    Container(child: Expanded(child: TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Email Address',), controller: emailFieldController),))],)),

                  Container(height: 1.0, color: Colors.grey),


                  Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                    Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Icon(Icons.lock)),
                    Container(child: Expanded(child: TextField(obscureText: true, decoration: InputDecoration(border: InputBorder.none, hintText: 'Password',), controller: passwordFieldController),))],)),


                  // Password Field
                  //Container(child: Row(children: <Widget>[TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Password',), controller: passwordFieldController,)],))


                ],),),


              // LOGIN BUTTON
              Container(margin: EdgeInsets.only(top: size.height * 0.05),
                  width: size.width * 0.8,
                  height: size.height * 0.05,child: LoginBtn()

              ),

              Container(width: size.width * 0.5, height: 1.0,margin: EdgeInsets.only(top: size.height * 0.02),color: Colors.white),


              // FACEBOOK BUTTON
              Container(margin: EdgeInsets.only(top: size.height * 0.02),
                width: size.width * 0.8,
                height: size.height * 0.05,child:
                RaisedButton(
                  elevation: 4.0,
                  splashColor: Colors.blueGrey,
                  color: Colors.indigo,
                  child: Row( mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(margin: EdgeInsets.all(10.0), child: Image.asset("fb_icon.png", color: Colors.white,),),
                      Container(child: Text("Sign in with facebook", style: TextStyle(fontSize: 18.0, color: Colors.white), textAlign: TextAlign.center,)),],

                  ),

                  onPressed: (){

                    LoginBtn();

                    //InitApi(Config().BaseAPIURL+Config().API_LOGIN, params);
                    /*setState(() {


                                       initApi = true;
                                      _ShowProgress = true;

                                    });*/
                  },),),

              Container(margin: EdgeInsets.only(top: size.height * 0.03, ),
                  child: GestureDetector(onTap: (){

                    Navigator.of(contexts).push<Widget>(PageRouteBuilder<Widget>(
                        opaque: true,
                        transitionDuration: Duration(days: 1),
                        pageBuilder: (BuildContext context, _, __) {
                          return Container(
                            child: Signup(),
                          );
                        }));

                  }, child: ShadowText('Signup', style: TextStyle(color: Colors.red, fontSize: 18.0, fontWeight: FontWeight.bold),)))
              //Text("Signup", style: TextStyle(color: Colors.redAccent, fontSize: 22.0, fontWeight: FontWeight.bold),),)

            ],),),
        ]

          ,)
        ),

      ],),
    );


  }

}

class ShadowText extends StatelessWidget {

  final String data;
  final TextStyle style;

  ShadowText(this.data, { this.style }) : assert(data != null);

  Widget build(BuildContext context) {
    return new ClipRect(
      child: new Stack(
        children: [
          new Positioned(
            top: 2.0,
            left: 2.0,
            child: new Text(
              data,
              style: style.copyWith(color: Colors.white.withOpacity(0.7)),
            ),
          ),
          new BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
            child: new Text(data, style: style),
          ),
        ],
      ),
    );
  }
}

class LoginBtn extends StatefulWidget{

  bool dialog = false;
  LoginState createState() => new LoginState();

}

class LoginState extends State<LoginBtn>{

  void _showDialog() {
    setState(() {
      widget.dialog = true;
    });

    showDialog(
      context: context,
      barrierDismissible: false,
      child: new Container(
        color: Colors.black.withOpacity(0.5),
        child: showCircularIndicator(),
      ),
    ).then((_) {
      if (mounted) {
        setState(() {
          widget.dialog = false; // dialog was closed
        });
      }
    });

    HashMap ApiParams = new HashMap<String, String>();
    ApiParams.putIfAbsent("email",    () => emailFieldController.text.toString());
    ApiParams.putIfAbsent("password", () => passwordFieldController.text.toString());

    initApi(Config().API_BASE + Config().MODULE_LOGIN, ApiParams, context, true).PostApi().then((responseString){

      if (responseString != ""){

        if (responseString != "[]" && responseStatus == StatusOkay) {

          final jsonResponse = json.decode(responseString);

          PersonInfo myInfo = new PersonInfo.fromJson(jsonResponse);

          SuaraStorage().saveString(Config().LoginDetails, responseString);
          Config.access_token = myInfo.access_token;

          print(myInfo.access_token);
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) => MyFirstScreen()));

        } else {

          Navigator.of(context, rootNavigator: true).pop();

        }

      } else {

        Navigator.of(context, rootNavigator: true).pop();
      }

    });


  }

  @override
  Widget build(BuildContext contexts) {


    return RaisedButton(
      elevation: 4.0,
      splashColor: Colors.white,
      color: Colors.redAccent,
      child: Row( mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(child: Text("Login",
            style: TextStyle(fontSize: 18.0, color: Colors.white),
            textAlign: TextAlign.center,)),],

      ),

      onPressed: (){

            if (emailFieldController.text.toString() != "" && passwordFieldController.text.toString() != "") {
              _showDialog();
            }

      },);

  }
}

class Signup_login_btn extends StatefulWidget{

  Signup_login createState() => new Signup_login();

}

class Signup_login extends State<Signup_login_btn>{


  bool callApi = false;


  @override
  Widget build(BuildContext context) {

    return Container(child: Stack(

      children: <Widget>[

        Container(margin: EdgeInsets.only(right: 25.0) , child: RaisedButton(
          elevation: 4.0,
          splashColor: Colors.white,
          color: Colors.blueGrey,
          child: Row( mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(child: Text("Signup",
                style: TextStyle(fontSize: 18.0, color: Colors.white),
                textAlign: TextAlign.center,)),],

          ),

          onPressed: (){


          },)),



        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[Container(width: 50.0, height: 50.0,
          decoration: BoxDecoration(color: Colors.blue,
              borderRadius: new BorderRadius.circular(100.0),
              boxShadow:  [new BoxShadow(
                color: Colors.blueGrey,
                blurRadius: 20.0,
              )])
          ,
          child: Icon(FontAwesomeIcons.facebookF, color: Colors.white,),),])



      ],

    ),);

  }
}

class Signup extends StatefulWidget{

  File ImagePath = null;
  var bytes = null;
  bool Signup_ordinary = false;

  SignupState createState() => SignupState();

}

class SignupState extends State<Signup>{

  HashMap<String, String> ApiParams = new HashMap();


  submitForm() async {

    ApiParams.putIfAbsent("image", () => "data:image/png;base64,"+ base64Encode(widget.ImagePath.readAsBytesSync()));

    final response = await http.post(
      //https://test-botsphere.mobsocial.net/aduan1/web/pages/api/saveimg1.php?imgname="data:image/jpeg;base64,
      "https://test-botsphere.mobsocial.net/aduan1/web/pages/api/saveimg1.php",
      body: ApiParams,

    );

    print(response.body);

  }


  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return MaterialApp(home: Scaffold(backgroundColor: Colors.transparent,body: Stack(children: <Widget>[
      new BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: new Container(
          decoration: new BoxDecoration(
              color: Colors.black.withOpacity(0.7)
          ),

        ),
      ),

      Center(child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

        GestureDetector(child: Container(width: size.width * 0.23, height: size.width * 0.23, decoration: BoxDecoration(
            borderRadius: new BorderRadius.circular(100.0),
            image: DecorationImage(
                alignment: FractionalOffset.center,
                image: widget.ImagePath == null ? new AssetImage("assets/camera_icon.png") : FileImage(widget.ImagePath),
                fit: BoxFit.fill)

        ),), onTap: () async{


          File img = await ImagePicker.pickImage(source: ImageSource.gallery);

          //;;String imgS = await rootBundle.loadString("assets/camera_icon.png");


        //  setState(() async {

            widget.ImagePath = img;

            Dio dio = new Dio();

          /*
            FormData formdata = new FormData(); // just like JS

            formdata.add("file", new UploadFileInfo(img, "profile_image.png"));

            dio.post("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos", data: formdata, options: Options(
                method: 'POST',
                responseType: ResponseType.PLAIN // or ResponseType.JSON
            ))
                .then((response){

                  print("Hehehhe123 -- " + response.data.toString());

            })
                .catchError((error) => print(error));*/

          String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

          FormData formData = new FormData.from({"file": new UploadFileInfo(img, "upload1.jpg")});

          var response = await dio.post("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos?access_token=rNC356KjKD26yRfqy1xfCGdK",
              data: formData,
              options: Options(headers: {"Accept":"application/json", "Authorization": auth}));


          print("JUK "+ response.toString());

            //String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

/*
            var uri = Uri.parse("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos");

            var request = new http.MultipartRequest("POST", uri, );
                request.headers.addAll({"Accept":"application/json", "Authorization": auth});
                request.fields.addAll({"access_token":"rNC356KjKD26yRfqy1xfCGdK"});


            request.files.add(multipartFile);
            var response = await request.send();
            print(response.statusCode);
            response.stream.transform(utf8.decoder).listen((value) {

              print("ehhehe " + value);


            });*/


         /*  http.MultipartFile.fromBytes('file', )

           var multipartFile = new http.MultipartFile('file', stream, length,
              filename: "profile_image.png",
              contentType: new MediaType('file', 'image/jpeg'));
*/

            /*var request = new http.MultipartRequest("POST", Uri.parse("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos"));
            request.fields['access_token'] = 'rNC356KjKD26yRfqy1xfCGdK';
            request.headers.addAll({"Accept":"application/json", "Authorization": auth});
            request.files.add(multipartFile);

            request.send().then((response) {

              if (response.statusCode == 200) print("Uploaded!");

              else response.stream.transform(utf8.decoder).listen((value) {

                  print("ehhehe " + value);

                });

            });*/

         /* var stream = new http.ByteStream(DelegatingStream.typed(img.openRead()));
          var length = await img.length();
*/

          /*http.MultipartRequest request =
            new http.MultipartRequest('POST', Uri.parse("https://myyb2.mobsocial.net/fan-api/1.0/me/profile-photos"));

            request.fields['access_token'] = 'rNC356KjKD26yRfqy1xfCGdK';
            request.headers.addAll({"Accept":"application/json", "Authorization": auth});

            request.files.add(
                new http.MultipartFile.fromBytes(
                  'file',
                  img.readAsBytesSync(),
                  filename: 'profile_image.png', // optional
                  contentType: new MediaType('image', 'jpeg'),
                ),
            );

            http.StreamedResponse r = await request.send();
            print("Hello "+ r.statusCode.toString());

            r.stream.transform(utf8.decoder).listen((value) {

              print("ehhehe " + value);

            });*/

             /* HashMap<String, String> ApiParams = new HashMap();
                ApiParams.putIfAbsent("access_token", ()=> "rNC356KjKD26yRfqy1xfCGdK");

              http.Response res = await http.get("https://myyb2.mobsocial.net/fan-api/1.0/artists/featured?access_token=rNC356KjKD26yRfqy1xfCGdK",
                  headers: {"Accept":"application/json", "Authorization": auth} ); // post api call


              if (res.statusCode == 200) {

                print("Success "+res.body.toString());


              } else {

                print("Error "+res.body.toString());

              }*/


            /*

            final response = await http.post(

              headers: {"Accept":"application/json", "Authorization": auth},
              body: {
                "access_token" : "rNC356KjKD26yRfqy1xfCGdK"
                //'file': img.path,
              },
            );*/

            // print("ehhehe " + response.body);


            List<int> imageBytes = img.readAsBytesSync();

            print("Image Byte" + imageBytes.toString());

            print("Image Byte AWSOM" + imageBytes.toString());

            String base64Image = base64Encode(imageBytes);

            //submitForm();

            //submitForm(base64Image);

            print("Hello "+ img.toString());

         // });

        },),



        Container(margin: EdgeInsets.only(top: size.height * 0.08), width: size.width * 0.8, decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.circular(size.width * 0.01)),
          child:  Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

            //Email Field
            Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
              Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Icon(Icons.perm_identity)),
              Container(child: Expanded(child: TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Name',), controller: nameFieldController),))],)),

            Container(height: 1.0, color: Colors.grey),

            // Password Field
            //Container(child: Row(children: <Widget>[TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Password',), controller: passwordFieldController,)],))


          ],),),


        Container(margin: EdgeInsets.only(top: size.height * 0.08), height: size.height * 0.5, width: double.infinity, child:  Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[ // width: size.width*2, height: size.width*2,child: Icon(Icons.lock_outline),

            Container(width: size.width * 0.8, height: 100.0,  decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.circular(size.width * 0.01)),
              child:  Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

                //Email Field
                Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Icon(Icons.email)),
                  Container(child: Expanded(child: TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Email Address',), controller: emailFieldController),))],)),

                Container(height: 1.0, color: Colors.grey),

                Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Container(margin: EdgeInsets.fromLTRB(size.width*0.02, 0.0, size.width*0.05, 0.0), child: Icon(Icons.lock)),
                  Container(child: Expanded(child: TextField(obscureText: true, decoration: InputDecoration(border: InputBorder.none, hintText: 'Password',), controller: passwordFieldController),))],)),


                // Password Field
                //Container(child: Row(children: <Widget>[TextField(decoration: InputDecoration(border: InputBorder.none, hintText: 'Password',), controller: passwordFieldController,)],))


              ],),),


            Container(width: size.width * 0.5, height: 1.0,margin: EdgeInsets.only(top: size.height * 0.05),color: Colors.white),

            // LOGIN BUTTON
            Container(margin: EdgeInsets.only(top: size.height * 0.05),
                width: size.width * 0.8, child: Signup_login_btn()),


            Container(margin: EdgeInsets.only(top: 25.0),child: GestureDetector(child: Center(child: Icon(Icons.close, color: Colors.white, ),), onTap: (){

              Navigator.pop(context);

            } ,))

            //Text("Signup", style: TextStyle(color: Colors.redAccent, fontSize: 22.0, fontWeight: FontWeight.bold),),)

          ],),),




      ],),



      )

    ],)),);
  }

}




