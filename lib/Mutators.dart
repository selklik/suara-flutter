


import 'dart:async';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class Config {

  String APIKey;
  String APISecret;

  Config(){

    if (Platform.isAndroid) {

      APIKey = "432682030147966";
      APISecret = "55ff2d0544e91fd5c5d1ff4d7675619e";

    } else if (Platform.isIOS) {

      APIKey = "432682030147966";
      APISecret = "55ff2d0544e91fd5c5d1ff4d7675619e";

    }

  }

  String OneSignalAppID = "d13da41f-0e33-4c18-a282-5ee249c21059";

  String API_BASE = "http://myyb2.mobsocial.net/fan-api/1.0";

  String MODULE_LOGIN = "/auth/login";
  String MODULE_MPS_LIST = "/artists/featured";
  String MODULE_ME_FOLLOWING = "/me/following";
  String MODULE_FEED = "/me/feed";
  String MODULE_POST = "/posts/";
  String MODULE_LIKES = "/likes";

  String API_PROFILE_PIC = "/me/profile-photos";
  String API_SIGNUP = "/auth/signup";

  String API_LOGOUT = "/auth/logout";


  static String access_token = "";

  // Shared Preferences -- //

  String LoginDetails = "st_logindetails";
  String MpDetails    = "st_mpdetails";

  String tosUrl = "https://myyb2.mobsocial.net/pages/tos";
  String privacyUrl = "https://myyb2.mobsocial.net/pages/privacy";
  String faqUrl = "https://myyb2.mobsocial.net/pages/faq";


  //------- Complain Api -------- //
  String complainList = "https://test-botsphere.mobsocial.net/aduan1/web/pages/api/read_cat2.php#";

  String me_ComplainList = "https://test-botsphere.mobsocial.net/aduan1/web/pages/api/read_adu.php";

  String me_PostComplain = "https://test-botsphere.mobsocial.net/aduan1/web/pages/api/saveapi.php";


}


class SuaraStorage{

  void saveString(String key, String name) async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, name);

  }

  Future<String> getString(String key) async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getString(key) ?? "";

  }


}

