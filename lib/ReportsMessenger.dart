
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:suara/ApiHelper.dart';
import 'package:suara/Models.dart';
import 'package:suara/Mutators.dart';
import 'package:http/http.dart' as http;


class initReportsMessengerLayout extends StatelessWidget{

  String Category;
  String SubCategory;
  String ArtistId;

  initReportsMessengerLayout(this.Category, this.SubCategory, this.ArtistId);

  @override
  Widget build(BuildContext context) {

    print("Helll ooo o -===== "+ SubCategory);

    return Scaffold(appBar: AppBar(
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      backgroundColor: Colors.white,
      title: Text("Complain Box", style: TextStyle(fontFamily: "Bold", color: Colors.black),),),
      body: Container(color: Colors.white, child: ReportsMessenger(Category, SubCategory, ArtistId)),
    );
  }
}

class ReportsMessenger extends StatefulWidget{

  String Category = "";
  String SubCategory = "";
  String username = "";
  String userProfile = "";
  String ArtistId = "";

  PersonInfo myInfo = null;
  int userid = 0;

  ReportsMessenger(this.Category, this.SubCategory, this.ArtistId);

  ReportsMessengerState createState () => ReportsMessengerState();


}

final messageFieldController = TextEditingController();


class ReportsMessengerState extends State<ReportsMessenger>{


  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    if (widget.userid == 0) {
      SuaraStorage().getString(Config().LoginDetails).then((value){

        setState(() {

          final jsonResponse = json.decode(value);
          widget.myInfo = new PersonInfo.fromJson(jsonResponse);
          widget.userid = widget.myInfo.id;
          widget.username = widget.myInfo.name;
          widget.userProfile = widget.myInfo.profile_photos.thumbnail;

        });

      });
    }

    return Container(child:  Column(children: <Widget>[

      Expanded(child: Container(color: Colors.white, child: ComplainList(widget.userid.toString().trim(), widget.username, widget.userProfile),)),

      Container(height: screen.height * 0.08, child: Stack(children: <Widget>[ Container(color: Colors.white,

        child: Column(children: <Widget>[

          Container(height: 1.0, color: Colors.grey, width: screen.width,),

          Row(children: <Widget>[

          Expanded(child: Container(margin: EdgeInsets.only(left: 20.0),
                   child: TextField(controller: messageFieldController,
                   decoration: InputDecoration(hintText: "Enter Your Message", border: InputBorder.none),))),

          SelectImageBtn(),

          Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

            IconButton(iconSize: screen.width * 0.05, icon: Icon(Icons.send, color: Colors.blueGrey),

              onPressed: (){

                if (messageFieldController.text != ""){

                    HashMap<String, String> ApiParams = new HashMap();

                    ApiParams.putIfAbsent("mpid", ()=> widget.ArtistId);
                    ApiParams.putIfAbsent("category", ()=> widget.Category);
                    ApiParams.putIfAbsent("subCategory", ()=> widget.SubCategory);
                    ApiParams.putIfAbsent("message", ()=> messageFieldController.text.trim());
                    ApiParams.putIfAbsent("userid", ()=> widget.userid.toString().trim());
                    ApiParams.putIfAbsent("username", ()=> widget.username);
                    ApiParams.putIfAbsent("imagepath", () => ImagePath != null ? "data:image/png;base64,"+ base64Encode(ImagePath.readAsBytesSync()) : "");

                    initApi(Config().me_PostComplain, ApiParams, context, true).PostMultiApi().then((reponse){

                        setState(() {});

                    });

                }

              },)

          ],),),
        ],)

      ],),)],)),


    ],),

    );

  }

}


class ComplainList extends StatefulWidget{

  String userId = "";
  String userName = "";
  String userProfile=  "";
  String responseString = "";

  List<dynamic> myComplainList = new List();

  ComplainList(this.userId, this.userName, this.userProfile);

  ComplainList_State createState () => ComplainList_State();

}

class ComplainList_State extends State<ComplainList>{

  Future _initRefresh() async {

    HashMap<String, String> ApiParams = new HashMap();

    initApi(Config().me_ComplainList + "?id=" +widget.userId, ApiParams, context, true).PostApi().then((responseString){

      setState(() {

        if (responseStatus == StatusOkay && responseString != ""){

          widget.responseString = responseString;

          widget.myComplainList = json.decode(responseString);

        }

      });

    });

    Completer<Null> completer = new Completer<Null>();

    new Future.delayed(new Duration(seconds: 3)).then((_){

      completer.complete();

    });

    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    ScrollController _controller = ScrollController();


    final screen = MediaQuery.of(context).size;

    var noComments = Container(child: Center(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(width: screen.width * 0.2, height: screen.width * 0.2,
          child: Image.asset("no_message.jpg"),),
        Container(margin: EdgeInsets.all(screen.width * 0.05), child: Text("No Comments yet!"),)
      ],)));


    final refreshIndicator = new RefreshIndicator(child: ListView.builder(
      controller: _controller,
      itemBuilder: (BuildContext context, int Index){

        return Container(margin: EdgeInsets.fromLTRB(10.0, screen.height * 0.01, 10.0, 5.0),
          child: new MessagesFeeds(Index,
            widget.myComplainList[Index]["category"],
            widget.myComplainList[Index]["categoryname"],
            widget.myComplainList[Index]["message"],
            widget.myComplainList[Index]["imagepath"],
            widget.userName,
            widget.userProfile,
            widget.myComplainList[Index]["tstamp"],), );

      },
      itemCount: widget.myComplainList.length,

    ), onRefresh: _initRefresh,);


    HashMap<String, String> ApiParams = new HashMap();

      print("Hello"+ widget.userId);

      return widget.myComplainList.length == 0 ? FutureBuilder(future: initApi(Config().me_ComplainList + "?id=" +widget.userId, ApiParams, context, false).PostApi(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {

          if (snapshot.hasData){

            if (snapshot.data != "[]"){


                lastCategory = "";

                widget.responseString = snapshot.data;
                widget.myComplainList = json.decode(snapshot.data);

                //Timer(Duration(milliseconds: 1000), () => _controller.jumpTo(_controller.position.maxScrollExtent));

                return new RefreshIndicator(child: ListView.builder(
                  controller: _controller,
                  itemBuilder: (BuildContext context, int Index){

                    return Container(margin: EdgeInsets.fromLTRB(10.0, screen.height * 0.01, 10.0, 5.0),
                      child: new MessagesFeeds(Index,
                        widget.myComplainList[Index]["category"],
                        widget.myComplainList[Index]["categoryname"],
                        widget.myComplainList[Index]["message"],
                        widget.myComplainList[Index]["imagepath"],
                        widget.userName,
                        widget.userProfile,
                        widget.myComplainList[Index]["tstamp"],), );

                  },
                  itemCount: widget.myComplainList.length,

                ), onRefresh: _initRefresh,);

            } else{

              return Container(child: Center(child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

                      Image.asset("assets/no_message.jpg", width: screen.width * 0.2, height: screen.width * 0.2,),
                      Container(margin: EdgeInsets.all(15.0), child: Text("You have not complained about anything yet!",
                                style: TextStyle(fontFamily: "regular", color: Colors.grey),),)

                    ],),),);

            }

          } else {

            return Container(child: showCircularIndicator(),);

          }


        }) : refreshIndicator;


  }

}

String lastCategory = "";


class MessagesFeeds extends StatelessWidget {

  int Index;
  String category;
  String categoryName;
  String message;
  String imagepath;
  String iconDepartment;
  String tstamp;

  String userName;
  String userProfile;


  MessagesFeeds(this.Index, this.category, this.categoryName, this.message, this.imagepath, this.userName, this.userProfile, this.tstamp);

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    if (category == "Kebajikan")
      iconDepartment = "assets/cp_welfare.png";

    if (category == "Bencana alam")
      iconDepartment = "assets/cp_environment.png";

    if (category == "Infrastructure") //Infrastruktur & prasarana
      iconDepartment = "assets/cp_infrastructure.png";

    if (category == "Communism") //Kepenggunaan
      iconDepartment = "assets/cp_community.png";

    if (category == "Kesihatan")
      iconDepartment = "assets/cp_health.png";

    if (category == "Asan Tani")
      iconDepartment = "assets/cp_basfar.png";


    final ifTopicChanged = Container(child: Column(children: <Widget>[

        Container(child: Center(child: Image.asset(iconDepartment, width: screen.width * 0.1, height: screen.width * 0.1,),),),

        Container(decoration: BoxDecoration(color: Colors.grey.withOpacity(0.2), borderRadius: BorderRadius.circular(200.0)),margin: EdgeInsets.only(top: screen.height * 0.02, bottom: screen.height * 0.05),
            child: Container(margin: EdgeInsets.only(left: screen.width * 0.025, right: screen.width * 0.025, bottom: 5.0, top: 5.0, ),child: Text(userName+ " sets the topic to \""+category+"\"", style: TextStyle(color: Colors.black, fontFamily: "Light", fontSize: screen.height * 0.012),)),),
    
    ],),);

    final userProfileDate = Container(margin: EdgeInsets.only(top: screen.height * 0.03), child: Row(children: <Widget>[

      Container(width: screen.width * 0.12, height: screen.width * 0.12,
          decoration: new BoxDecoration(image: DecorationImage(image: new NetworkImage(userProfile),
          fit: BoxFit.fill),borderRadius: new BorderRadius.circular(50.0))),

      Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(height: 20.0, margin: EdgeInsets.only(left: 5.0),child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

          Text(userName, style: TextStyle(color: Colors.black, fontFamily: "Bold")),
          Container(margin: EdgeInsets.only(left: 5.0), child: Text(tstamp, style: TextStyle(color: Colors.grey, fontFamily: "Light", fontSize: screen.height * 0.015)),)

          ],),
        ),


      ],)

    ],),);

    final MessageContainer = Container(width: screen.width * .8,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Container(decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(screen.height * 0.02)),
            child: Container(margin: EdgeInsets.only(left: 25.0, right: 10.0),
              child: Text(message, style: TextStyle(color: Colors.black, fontFamily: "Regular"),),))

      ],),);

    final onlyMessageBody = Container(child: Column(children: <Widget>[

      lastCategory != category ? ifTopicChanged : Container(),
      userProfileDate,
      MessageContainer,
      ImageAnim(imagepath, Index),

    ],));


    lastCategory = category;

    return  imagepath != null ? onlyMessageBody : Container();

  }

}

File ImagePath = null;
HashMap<String, bool> visible = new HashMap();

class ImageAnim extends StatefulWidget{

  String imagePath;
  bool _visible = false;
  bool downloadImage = false;
  int Index = 0;

  ImageAnim(this.imagePath, this.Index);

  ImageAnim_State createState() => new ImageAnim_State();

}

class ImageAnim_State extends State<ImageAnim>{


  void initAnim(){

    setState(() {
      widget._visible = true;
    });

  }

  double opacityLogic(){

    if (widget._visible){

      widget._visible = true;

      return 1.0;

    } else {

      return 0.0;
    }

  }

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    initAnim();

    return AnimatedOpacity(
            opacity: opacityLogic(),
            duration: Duration(milliseconds: 200),
            child: Stack(children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: screen.height * 0.02),
              width: screen.width * .7, height: screen.width,
              decoration: BoxDecoration(image: DecorationImage(
                      image: CachedNetworkImageProvider(widget.imagePath),
                      fit: BoxFit.fill), borderRadius: new BorderRadius.circular(screen.width * 0.02)),
              child: !widget.downloadImage ? new BackdropFilter(
                  filter: new ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: new Container(
                    decoration: new BoxDecoration(color: Colors.grey.withOpacity(0.5),borderRadius: new BorderRadius.circular(screen.width * 0.02)),
                  )) : Container(),
            ), Container(width: screen.width * .7, height: screen.width, child: Center(child:
                        !widget.downloadImage ? GestureDetector(child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Container(margin: EdgeInsets.all(5.0), child: Icon(Icons.view_agenda, color: Colors.white,),),
                        Container(margin: EdgeInsets.all(5.0), child: Text("View", style: TextStyle(fontFamily: "Regular", color: Colors.white)),),
                    ],),
                             width: screen.width * 0.35, height: screen.height * 0.05, decoration: BoxDecoration(color: Colors.grey.withOpacity(0.5),
                             borderRadius: BorderRadius.circular(200.0,)),), onTap: (){

                     setState(() {widget.downloadImage = true;});

                   },): Container(),),)

            /*Container(
                margin: EdgeInsets.only(top: screen.height * 0.02),
                width: screen.width * .7, height: screen.width,
                decoration: new BoxDecoration(
                    image: DecorationImage(image: new CachedNetworkImageProvider(widget.imagePath),
                        fit: BoxFit.cover), borderRadius: new BorderRadius.circular(screen.width * 0.02))),*/


            ],));

  }

}

class SelectImageBtn extends StatefulWidget{


  SelectImage_State createState() => SelectImage_State();

}

class SelectImage_State extends State<SelectImageBtn>{

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

      IconButton(iconSize: screen.width * 0.05, icon: Icon(FontAwesomeIcons.clipboard, color: ImagePath == null ? Colors.blueGrey :  Colors.red),
        onPressed: () async{

          File img = await ImagePicker.pickImage(source: ImageSource.gallery);

          setState(() {

            ImagePath = img;

            List<int> imageBytes = img.readAsBytesSync();
            print(imageBytes);

            String base64Image = base64Encode(imageBytes);

          });

        },)

    ],),);

  }

}
