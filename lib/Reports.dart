import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:suara/ApiHelper.dart';
import 'package:suara/Models.dart';
import 'package:suara/Mutators.dart';
import 'package:suara/ReportsMessenger.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';


String artistid = "";


class AnimText extends StatefulWidget{

  String artistMp = "";
  AnimText(this.artistMp);

  bool visible = false;

  AnimText_State createState() => new AnimText_State();
}

class AnimText_State extends State<AnimText>{
  Timer _timer;

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {

    _timer = new Timer(const Duration(milliseconds: 500), () {
      setState(() {
        widget.visible = true;
      });
    });

    final screen = MediaQuery.of(context).size;

    return AnimatedOpacity(
      opacity: widget.visible ? 1.0 : 0.0,
      duration:Duration(milliseconds: 200),
      // The green box needs to be the child of the AnimatedOpacity
      child: IgnorePointer(ignoring: true,
        child: Container(margin: EdgeInsets.only(top: 40.0),
          width: screen.width, height: 300.0, child: Container(
          alignment: Alignment.bottomCenter, child: Text(widget.artistMp),
        ),),),
    );
  }
}

class Carroussel extends StatefulWidget {

  List<MpsPersonalInfo> mpList;

  Carroussel(this.mpList);

  @override
  _CarrousselState createState() => new _CarrousselState();
}

class _CarrousselState extends State<Carroussel> {
  var screen;

  PageController controller;
  int currentpage = 0;

  @override
  initState() {
    super.initState();
    controller = new PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 0.5,
    );
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screen = MediaQuery.of(context).size;

    return new Stack(children: <Widget>[

      Center(child: new Container(margin: EdgeInsets.only(bottom: screen.height * 0.04),
          child: new PageView.builder(
          onPageChanged: (value) {
            setState(() {
              currentpage = value;
                artistid = widget.mpList[value].id.toString().trim();
            });
          },
          controller: controller,
          itemBuilder: (context, index) => builder(index)),
        ),
      ),
     
      Container(margin: EdgeInsets.only(top: screen.width * 0.04),
          child:  AnimText(widget.mpList[currentpage].name))

    ],);
  }

  builder(int index) {
    return new AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page - index;
          value = (1 - (value.abs() * .5)).clamp(0.0, 1.0);
        }

        return new Center(
          child: new SizedBox(
            height: Curves.easeOut.transform(value) * screen.width * 0.3,
            width: Curves.easeOut.transform(value) * screen.width * 0.3,
            child: child,
          ),
        );
      },
      child: new Container(
        margin: const EdgeInsets.all(8.0),
        child: Container(
          width: screen.width * 0.15, height: screen.width * 0.15,
          decoration: new BoxDecoration(image: new DecorationImage(
              image: new CachedNetworkImageProvider(widget.mpList[index].profile_photos.square),
              fit: BoxFit.fill), borderRadius: new BorderRadius.circular(200.0)),),
      ),
    );
  }
}

class MainReports extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    var noReports = Container(child: Center(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(width: screen.width * 0.2, height: screen.width * 0.2,
          child: Image.asset("assets/speak_icon.png", color: Colors.black54,),),
        Container(margin: EdgeInsets.all(screen.width * 0.05),
          child: Text("No Reports", style: TextStyle(fontSize: screen.height * 0.03, color: Colors.black54),),)
      ],)));

    HashMap<String, String> ApiParams = new HashMap();
    ApiParams.putIfAbsent("access_token", () => Config.access_token);

    return Container(child: Stack(children: <Widget>[

      Column(children: <Widget>[

        Container(height: screen.height * 0.08, child: Center(child: Text("Direct Complains", style: TextStyle(fontFamily: "Bold", color: Colors.black, fontSize: 18.0),),),),

        Container(height: screen.height * 0.3, color: Colors.grey, child: Container(child: Center(

          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[

            Container(child: Text("Talk to your Mp/Adun", style: TextStyle(fontFamily: "Bold", color: Colors.white),)),
            Container(width: screen.width * 0.8, margin: EdgeInsets.only(top: screen.height * 0.02), child:
            Text("Your MP/Aduan will listen to your complains and concerns.",  textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontFamily: "Light"),)),

          ],),
        )),),

        Container(margin: EdgeInsets.only(top: screen.width * 0.05), height: screen.height * 0.15, alignment: Alignment.bottomCenter, child: Text("Choose from the list of Mp/Adun relevant for your query", style:  TextStyle(fontFamily: "Light"),),),

        Expanded(child: Container(child: Center(child: new RaisedButton(color: Colors.red,
            child: new Container(width: screen.width * 0.5, height: screen.height * 0.05,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0)),
              child: Stack(children: <Widget>[
                Container(margin: EdgeInsets.only(left: screen.width * 0.02), child: Column(mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Icon(Icons.send, color: Colors.white, size: screen.width * 0.05,)],)),
                Container(child: Center(child: Text("Send a Message", style: TextStyle(color: Colors.white, fontFamily: "Regular"),)))

              ],),),
            onPressed: (){

              Navigator.push(context, MaterialPageRoute(builder: (context) => ComplainList()));

            },
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
        ))))
        //RaisedButton.icon(onPressed: (){}, icon: Icon(Icons.send, color: Colors.white,), label: Text("Send a Message")),),))

      ],),

      // Horizontal Scrollview
      Container(margin: EdgeInsets.only(top: screen.height * 0.29), child: Container(height: screen.height * 0.22,
          child: FutureBuilder(future: initApi(Config().API_BASE + Config().MODULE_MPS_LIST, ApiParams, context, false).GetApi(),
              builder: (BuildContext context, AsyncSnapshot response){

                if (response.hasData){

                  var decoded = json.decode(response.data);
                  States states = States.fromJson(decoded);

                  List<MpsPersonalInfo> mpsInfo = states.Kedah;

                  return Stack(children: <Widget>[

                    IgnorePointer(ignoring: true, child: Container(margin: EdgeInsets.only(bottom: screen.height * 0.04), child: Center(
                      child: Container(width: screen.width * 0.3, height: screen.width * 0.3,
                        decoration: new BoxDecoration(boxShadow: [new BoxShadow(
                          color: Colors.grey.withOpacity(0.9),
                          blurRadius: 5.0,),],
                            border: Border.all(color: Colors.white, width: 2.0),
                            borderRadius: new BorderRadius.circular(200.0)),
                      ),


                    ))),

                    Container(margin: EdgeInsets.only(left: screen.width * 0.05, right: screen.width * 0.05),
                        color: Colors.transparent,
                        width: double.infinity,
                        child: Carroussel(mpsInfo)),

                    /*IgnorePointer(ignoring: true,
                      child: Container(width: screen.width, height: 300.0, child: Container(
                        alignment: Alignment.bottomCenter, child: Text("Hello"),
                      ),),),
*/
                    IgnorePointer(ignoring: true, child: Container(margin: EdgeInsets.only(bottom: screen.height * 0.04), child: Row(children: <Widget>[

                      Expanded(child: Column(children: <Widget>[

                        Expanded(child: Container(
                          decoration: new BoxDecoration(gradient: new LinearGradient(                                 // new
                            // Where the linear gradient begins and ends                                // new
                            // Add one stop for each color.
                            // Stops should increase
                            // from 0 to 1
                            stops: [0.1, 0.6],
                            colors: [
                              // Colors are easy thanks to Flutter's
                              // Colors class.
                              Colors.grey,
                              Colors.grey.withOpacity(0.0)
                            ],
                          ),
                          ),

                        )),
                        Expanded(child: Container(
                          decoration: new BoxDecoration(gradient: new LinearGradient(                                 // new
                            // Where the linear gradient begins and ends                                // new
                            // Add one stop for each color.
                            // Stops should increase
                            // from 0 to 1
                            stops: [0.1, 0.6],
                            colors: [
                              // Colors are easy thanks to Flutter's
                              // Colors class.
                              Colors.white,
                              Colors.white.withOpacity(0.0)
                            ],
                          ),
                          ),

                        ))

                      ],)),
                      Expanded(child: Column(children: <Widget>[

                        Expanded(child: Container(
                          decoration: new BoxDecoration(gradient: new LinearGradient(                                 // new
                            // Where the linear gradient begins and ends                                // new
                            // Add one stop for each color.
                            // Stops should increase
                            // from 0 to 1
                            stops: [0.4, 0.9],
                            colors: [
                              // Colors are easy thanks to Flutter's
                              // Colors class.

                              Colors.grey.withOpacity(0.0),
                              Colors.grey,

                            ],
                          ),
                          ),

                        )),
                        Expanded(child: Container(
                          decoration: new BoxDecoration(gradient: new LinearGradient(                                 // new
                            // Where the linear gradient begins and ends                                // new
                            // Add one stop for each color.
                            // Stops should increase
                            // from 0 to 1
                            stops: [0.4, 0.9],
                            colors: [
                              // Colors are easy thanks to Flutter's
                              // Colors class.
                              Colors.white.withOpacity(0.0),
                              Colors.white,
                            ],
                          ),
                          ),

                        ))

                      ],)),

                    ],),))

                  ],);



                  //return ScrollsByTime(mpsInfo: mpsInfo);

                }else{

                  return Center(child: showCircularIndicator(),);

                }

              })
      )
      )

      /*ListView(
              // This next line does the trick.
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(25.0),
                  width: 160.0,
                  color: Colors.red,
                ),
                Container(
                  margin: EdgeInsets.all(25.0),
                  width: 160.0,
                  color: Colors.blue,
                ),
                Container(
                  margin: EdgeInsets.all(25.0),
                  width: 160.0,
                  color: Colors.green,
                ),
                Container(
                  margin: EdgeInsets.all(25.0),
                  width: 160.0,
                  color: Colors.yellow,
                ),
                Container(
                  margin: EdgeInsets.all(25.0),
                  width: 160.0,
                  color: Colors.orange,
                ),
              ],
            ),*/


    ],),);


    Container(color: Colors.white, child: Column(children: <Widget>[

      Container(width: double.infinity, height: 50.0, child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
        Text("Reports", textAlign: TextAlign.center, style: TextStyle(fontSize: 16.0),)],),
        decoration: BoxDecoration(border: new Border(bottom: new BorderSide(width: 1.0, color: Colors.grey))),),
      Expanded(child:

      Container(color: Colors.white, child: noReports,)

      )

    ],));
  }
}


bool firstRun = false;


class ComplainList extends StatefulWidget{

  ComplainListState createState() => ComplainListState();

}

class ComplainListState extends State<ComplainList> with SingleTickerProviderStateMixin{

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {

    var screen = MediaQuery.of(context).size;

    HashMap<String, String> ApiParams = new HashMap();

    return Scaffold(appBar: AppBar(iconTheme: IconThemeData(color: Colors.black, ), backgroundColor: Colors.white, title: Text("Complain Regarding", style: TextStyle(fontFamily: "Regular", color: Colors.black),),),
        body: Container(child:

     Column(children: <Widget>[
      // Logo
      Container(height: screen.height * 0.15, child: Center(child: Container(width: screen.width * 0.2, height: screen.height * 0.2, child: Image.asset("assets/suara_logo.png"),),),),

      // Complain List
      Container(margin: EdgeInsets.all(screen.width * 0.02), height: screen.height * 0.5, child: Center(child:

      FutureBuilder(future: initApi(Config().complainList, ApiParams, context, false).PostApi(), builder: (BuildContext context, AsyncSnapshot response){

        if (firstRun){

          if (response.hasData){

            List<String> iconslist = new List();
            iconslist.add("assets/cp_welfare.png");
            iconslist.add("assets/cp_environment.png");
            iconslist.add("assets/cp_infrastructure.png");
            iconslist.add("assets/cp_community.png");
            iconslist.add("assets/cp_health.png");
            iconslist.add("assets/cp_basfar.png");

            List<dynamic> decode = json.decode(response.data);

            return GridView.count(
              shrinkWrap: true,
              // Create a grid with 2 columns. If you change the scrollDirection to
              // horizontal, this would produce 2 rows.
              crossAxisCount: 3,
              // Generate 100 Widgets that display their index in the List
              children: List.generate(decode.length, (index) {

                return Container(margin: EdgeInsets.all(4.0),
                  child: GestureDetector(child: Card(elevation: 4.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

                        Image.asset(iconslist[index], width: screen.width * 0.15, height: screen.width * 0.15,),
                        Text(decode[index]["Category"], textAlign: TextAlign.center, style: TextStyle(fontFamily: "Regular"),)

                      ],)), onTap:  (){

                      List<dynamic> subcategory = decode[index]["SubCategory"] as List<dynamic>;

                      List<Widget> ListMyWidgets(){

                        List<Widget> SubCategoryList = new List();

                        for (var i = 0; i < subcategory.length; i++){

                          SubCategoryList.add(CustomBottomDesign(subcategory[i], decode[index]["Category"]));

                        }

                        return SubCategoryList;

                      }

                      bool openOnce = true;

                      showModalBottomSheet(
                          context: context,
                          builder: (builder){

                            if (openOnce){

                                return new Container(
                                  color: Colors.transparent,
                                  child: new Container(
                                      height: screen.height * 0.4,
                                      decoration: new BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: new BorderRadius.only(
                                              topLeft: const Radius.circular(10.0),
                                              topRight: const Radius.circular(10.0))),
                                      child: Container(child: Column(children: <Widget>[

                                        Container(width: screen.width * 0.17, height: screen.width * 0.17, decoration: new BoxDecoration(
                                            image: DecorationImage(image: new AssetImage(iconslist[index]), fit: BoxFit.fill),borderRadius: new BorderRadius.circular(50.0))),

                                        Container(color: Colors.grey,
                                          margin: EdgeInsets.only(left: screen.width * 0.08, right: screen.width * 0.08, top: screen.width * 0.03, bottom: screen.width * 0.03), height: 1.0,),

                                        Expanded(child: Container(child: ListView(shrinkWrap: true, children:  ListMyWidgets(),)))

                                      ],))),
                                );

                            }


                            openOnce = false;
                          }
                      );




                      //BottomMenu();

                    /*Navigator.of(context).push<Widget>(PageRouteBuilder<Widget>(
                        opaque: true,
                        transitionDuration: Duration(days: 1),
                        pageBuilder: (BuildContext context, _, __) {
                          return Container(

                            child: SubComplain(decode[index]["SubCategory"] as List<dynamic>, decode[index]["Category"]),
                          );
                        }));*/

                  },),);
              },),
            );

          } else {

            return Container(height: screen.height * 0.6, child: Center(child: showCircularIndicator(),));

          }

        } else {

          firstRun = true;

          return Container(height: screen.height * 0.6, child: Center(child: showCircularIndicator(),));
        }

       }
      ),),),

      Expanded(child: Container(margin: EdgeInsets.all(screen.width * 0.05),
        alignment: Alignment.bottomCenter,
        child: Text(
          "Your complains and concerns will be considered closely for Malaysia's betterment.",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontFamily: "Regular"),),))


    ],),));

  }

}

class CustomBottomDesign extends StatelessWidget{

  String category;
  String subCategory;

  CustomBottomDesign(this.subCategory, this.category);

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    return Container(margin: EdgeInsets.all(10.0), child: Stack(

      children: <Widget>[
        Container(height: screen.height * 0.05,
          child: OutlineButton(
          borderSide: BorderSide(color: Colors.grey),
          highlightColor: Colors.white,
          highlightedBorderColor: Colors.red,
          color: Colors.white,
          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.only(topRight: Radius.circular(screen.width * 0.2), bottomRight: Radius.circular(screen.width * 0.2), bottomLeft: Radius.circular(screen.width * 0.2))),
          child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Shimmer.fromColors(
                baseColor: Colors.red,
                highlightColor: Colors.grey,
                child: Text(subCategory,
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                  textAlign: TextAlign.center,)
              ),

            ],

          ),

          onPressed: (){

            Navigator.push(context, MaterialPageRoute(builder: (context) => initReportsMessengerLayout(category, subCategory, artistid)));


          },)),

        Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[

          Container(width: screen.width * 0.1, height: screen.width * 0.1, child: Icon(Icons.keyboard_arrow_right, color: Colors.red, size: screen.width * 0.08,),),])


      ],

    ),);
  }

}

class BottomMenu extends StatefulWidget{

  BottomMenuState createState() => BottomMenuState();

}

class BottomMenuState extends State<BottomMenu> with SingleTickerProviderStateMixin{

  AnimationController controller;
  Animation<Offset> offset;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));

    offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 1.0))
        .animate(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: RaisedButton(
              child: Text('Show / Hide'),
              onPressed: () {
                switch (controller.status) {
                  case AnimationStatus.completed:
                    controller.reverse();
                    break;
                  case AnimationStatus.dismissed:
                    controller.forward();
                    break;
                  default:
                }
              },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SlideTransition(
              position: offset,
              child: Padding(
                padding: EdgeInsets.all(50.0),
                child: CircularProgressIndicator(),
              ),
            ),
          )
        ],
      ),
    );
  }

}
/*

class SubComplain extends StatefulWidget{

  List<dynamic> subcategory;
  String category;

  SubComplain(this.subcategory, this.category);

  SubComplain_State createState() => new SubComplain_State();

}

class SubComplain_State extends State<SubComplain>{

  @override
  Widget build(BuildContext context) {

    final screen = MediaQuery.of(context).size;

    for (int i = 0 ; i < widget.subcategory.length ; i++)
      print(widget.subcategory[i]);


    return MaterialApp(home: Scaffold(backgroundColor: Colors.transparent,body: Stack(children: <Widget>[

      BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: new Container(
          decoration: new BoxDecoration(
              color: Colors.grey.withOpacity(0.5)
          ),

        ),
      ),




      Container(child: Center(child: GridView.count(

        // Create a grid with 2 columns. If you change the scrollDirection to
        // horizontal, this would produce 2 rows.
        crossAxisCount: 2,
        shrinkWrap: true,
        // Generate 100 Widgets that display their index in the List
        children: List.generate(widget.subcategory.length, (index) {

          return Center(

            child: GestureDetector(
              child: Container(width: screen.width * 0.3, height: screen.width * 0.3, child:
              Stack(children: <Widget>[

                */
/*Container(width: screen.width * 0.3, height: screen.width * 0.3, decoration: new BoxDecoration(border: Border.all(color: Colors.red, width: screen.width * 0.05),
                      borderRadius: new BorderRadius.circular(screen.width)),),*//*



                Center(child: Container(width: screen.width * 0.28, height: screen.width * 0.28,
                  decoration: new BoxDecoration(
                      boxShadow:  [new BoxShadow(
                        color: Colors.black,
                        blurRadius: 20.0,
                      )],
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(screen.width)
                  )

                  , child: Container(width: screen.width * 0.275, height: screen.width * 0.27, child: Center(child: Text(widget.subcategory[index], textAlign: TextAlign.center, style: TextStyle(color: Colors.red, fontFamily: "Bold"),))),
                ),)


              ],)

                ,), onTap:  (){

              //Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) => initReportsMessengerLayout(widget.category, widget.subcategory[index], artistid)));

            },),);

        },),),),),

      Container(child: Center(child: Column(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[

        Container(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[

          IconButton(iconSize: screen.width * 0.08, icon: Icon(Icons.close, color: Colors.red),
            onPressed: (){

              Navigator.pop(context);

            },)

        ],),)

      ],)))

      ,

    ],)),);
  }
}
*/
