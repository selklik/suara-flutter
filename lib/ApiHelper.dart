
import 'dart:io';
import 'dart:ui';

import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:suara/Models.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:collection';
import 'package:suara/Mutators.dart';
import 'package:flutter/material.dart';


StatelessWidget ApiMoveClass;

String responseStatus;
String StatusOkay = "200";

class initApi{

  String responseApi;
  BuildContext context;
  HashMap<String, String> ApiParams;
  String url;
  bool showProgress;

  initApi(String url, HashMap<String, String> ApiParams, BuildContext context, bool showProgress){

    this.ApiParams = ApiParams;
    this.url = url;
    this.context = context;
    this.showProgress = showProgress;

  }

  Future<String> PostApi() async {

    if (showProgress) showLoadingScreen();

    String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

    http.Response res = await http.post(url, body: ApiParams, headers: {"Accept":"application/json", "Authorization": auth} ); // post api call

    if (res.statusCode == 200) {

      if (showProgress)
      Navigator.pop(context);

      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

    } else {


      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

      var errors = json.decode(res.body.toString().trim());

      if (showProgress)
        Navigator.pop(context);

      showError(errors["errors"][0]);

    }

    return responseApi;

  }

  Future<String> GetApi() async {

    String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

    http.Response res = await http.get(url +"?access_token="+ ApiParams["access_token"], headers: {"Accept":"application/json", "Authorization": auth} ); // post api call

    if (res.statusCode == 200) {

      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

    } else {

      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

    }

    return responseApi;

  }

  Future<String> GetApiFeeds() async {

    String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

    http.Response res = await http.get(url +"?access_token="+ ApiParams["access_token"]+"&page="+ ApiParams["page"], headers: {"Accept":"application/json", "Authorization": auth} ); // post api call

    if (res.statusCode == 200) {

      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

      //if (showProgress) Navigator.pop(context);

    } else {

      responseStatus = res.statusCode.toString().trim();
      responseApi = res.body.toString();

      // var errors = json.decode(res.body.toString().trim());
      //if (showProgress) Navigator.pop(context);
      // showError(errors["errors"][0]);


    }

    return responseApi;

  }

  Future<String> DeleteApi() async {

    String auth = "Basic " + base64Encode(utf8.encode(Config().APIKey + ":" + Config().APISecret));

    http.Response res = await http.delete(url +"?access_token="+ ApiParams["access_token"], headers: {"Accept":"application/json", "Authorization": auth} ); // post api call

    if (res.statusCode == 200) {

      responseApi = res.body.toString();
      print ("Response " + responseApi);


    } else {

      responseApi = res.body.toString();
      print ("Error " + responseApi);
    }

    return responseApi;

  }

  Future<String> PostMultiApi() async {

    if (showProgress) showLoadingScreen();

      print(ApiParams['imagepath']);

      final res = await http.post(
            url,
            body: ApiParams);

      if (res.statusCode == 200){

        if (showProgress)
          Navigator.pop(context);

      } else {

        if (showProgress)
          Navigator.pop(context);

        showError("Sorry! We couldn't proceed your complain.");

      }

      return res.body.toString();

  }

  void showError(String error){

    final screen = MediaQuery.of(context).size;

    showDialog(context: context,
        child: new AlertDialog(
      title: new Container(child: Center(child:
      Container(width: screen.width * 0.15, height: screen.width * 0.15, child: Image.asset("assets/error_icon.png", )),),),
      content: Container(margin: EdgeInsets.all(20.0), child: new Text(error, textAlign: TextAlign.center, style: TextStyle(fontFamily: "Regular"),),),
      )
    );

  }

  void showLoadingScreen(){

    Navigator.of(context).push<Widget>(PageRouteBuilder<Widget>(
        opaque: true,
        transitionDuration: Duration(minutes: 1),
        pageBuilder: (BuildContext context, _, __) {
          return Container(
            child: WidgetLoading(),
          );
        }));

  }

}


class WidgetLoading extends StatefulWidget{

  WidgetLoadingState createState() => WidgetLoadingState();

}

class WidgetLoadingState extends State<WidgetLoading>{


  @override
  Widget build(BuildContext context) {

    return MaterialApp(home: Scaffold(backgroundColor: Colors.transparent,body: Stack(children: <Widget>[
      new BackdropFilter(
      filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
      child: new Container(
          decoration: new BoxDecoration(
          color: Colors.black.withOpacity(0.5)
          ),

        ),
      ),
    new Container( child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        showCircularIndicator()

      ],),

    )])));
  }
}


class ShowProgress extends StatelessWidget{

  Widget progress = new Stack(children: <Widget>[
    Container(color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[new Center(
          child: new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),))],),

    )
  ],);

  Widget nprogress = new Container(
    child: new Container(),
  );

  @override
  Widget build(BuildContext context) {

    return progress;

  }

}