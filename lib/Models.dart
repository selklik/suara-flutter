import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show PlatformException;







/* Complain List */

class Subcategories{


}

class Categories{



}

class ComplainList{

}



/*  STATES MODELS */

class ProfilePhotos{

  final String full_resolution;
  final String square;
  final String thumbnail;

  ProfilePhotos({this.full_resolution, this.square, this.thumbnail});

  factory ProfilePhotos.fromJson(Map<String, dynamic> parsedJson){
    return ProfilePhotos(
      full_resolution: parsedJson['full_resolution'],
      square: parsedJson['square'],
      thumbnail: parsedJson['thumbnail'],
    );
  }

}

class SocialNetworkAccounts{

  final String facebook;
  final String twitter;
  final String instagram;

  SocialNetworkAccounts({this.facebook, this.twitter, this.instagram});

  factory SocialNetworkAccounts.fromJson(Map<String, dynamic> parsedJson){
    return SocialNetworkAccounts(
      facebook: parsedJson['facebook'],
      twitter: parsedJson['twitter'],
      instagram: parsedJson['instagram'],
    );
  }

}

class MpsPersonalInfo{

  final int id;
  final String name;
  final bool following;
  final int followers_count;
  final ProfilePhotos profile_photos;
  final String state;
  final String country_name;
  final String genre;
  final SocialNetworkAccounts social_network_accounts;
  final int registered_voters;

  MpsPersonalInfo({
      this.id, this.name, this.following, this.followers_count,
      this.profile_photos, this.state, this.country_name, this.genre, this.social_network_accounts,
      this.registered_voters});

  factory MpsPersonalInfo.fromJson(Map<String, dynamic> parsedJson){
    return MpsPersonalInfo(
      id: parsedJson['id'],
      name: parsedJson['name'],
      following: parsedJson['following'],
      followers_count: parsedJson['followers_count'],
      profile_photos: ProfilePhotos.fromJson(parsedJson['profile_photos']),
      state: parsedJson['state'],
      country_name: parsedJson['country_name'],
      genre: parsedJson['genre'],
      social_network_accounts: SocialNetworkAccounts.fromJson(parsedJson['social_network_accounts']),
      registered_voters: parsedJson['registered_voters'],

    );
  }

}

class States{

  List<MpsPersonalInfo> Kedah = [];

  States({this.Kedah});

  factory States.fromJson(Map<String, dynamic> parsedJson){

    try {

      return States(

          Kedah: (parsedJson['Kedah'] as List).map((i) {
          return MpsPersonalInfo.fromJson(i);
          }).toList() ?? [],


    );

    } on PlatformException catch (e) {

    }
  }

}

class PersonInfo{
  int id;
  String name;
  String email;
  String access_token;
  ProfilePhotos profile_photos;

  PersonInfo({this.id, this.name, this.email, this.access_token, this.profile_photos});

  factory PersonInfo.fromJson(Map<String, dynamic> parsedJson){
    return PersonInfo(
      id: parsedJson['id'],
      name: parsedJson['name'],
      email: parsedJson['email'],
      access_token: parsedJson['access_token'],
      profile_photos: ProfilePhotos.fromJson(parsedJson['profile_photos']),

    );
  }
}

/* ME FOLLOWING */
class MeFollowing {

  int count;
  List<MpsPersonalInfo> followings = [];

  MeFollowing({this.count, this.followings});

  factory MeFollowing.fromJson(Map<String, dynamic> parsedJson){
    return MeFollowing(
      count: parsedJson["count"],
      followings: (parsedJson["followings"] as List).map((i){
        return MpsPersonalInfo.fromJson(i);

      }).toList(),

    );

  }

}

class Complain {

  final String Category;
  final List<String> SubCategory;

  Complain({this.Category, this.SubCategory});

  factory Complain.fromJson(Map<String, dynamic> parsedJson){
    return Complain(
      Category: parsedJson["Category"],
      SubCategory: (parsedJson["SubCategory"] as List<String>)
    );

  }

}

class MyComplainList {

  final String id;
  final String mpid;
  final String category;
  final String categoryname;
  final String message;
  final String imagepath;
  final String status;
  final String tstamp;
  final String userid;
  final String username;

  MyComplainList({this.id, this.mpid, this.category, this.categoryname, this.message, this.imagepath, this.status, this.tstamp, this.userid, this.username});

  factory MyComplainList.fromJson(Map<String, dynamic> parsedJson){
    return MyComplainList(
      id: parsedJson["id"],
      mpid: parsedJson["mpid"],
      category: parsedJson["category"],
      categoryname: parsedJson["categoryname"],
      message: parsedJson["message"],
      imagepath: parsedJson["imagepath"],
      status: parsedJson["status"],
      tstamp: parsedJson["tstamp"],
      userid: parsedJson["userid"],

    );

  }

}


class Logout{

  final bool logged_out;

  Logout({this.logged_out});

  factory Logout.fromJson(Map<String, dynamic> parsedJson){
    return Logout( logged_out: parsedJson["logged_out"]);
  }

}


class showCircularIndicator extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    print("Created Progress");

    final screen = MediaQuery.of(context).size;
    return new Container(
        color: Colors.transparent,
        child: Center(child: Container(width: screen.width * 0.15, height: screen.width * 0.15,
          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.red)),)));
  }
}


